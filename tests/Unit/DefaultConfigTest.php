<?php

namespace Tests\Unit;

use Macrominds\Config\CommonConfig;
use Macrominds\Config\Env;
use Macrominds\ContentAccess\ContentPathProvider;
use Macrominds\DefaultConfig;
use Macrominds\Logger\LoggerFactory;
use Macrominds\Rendering\TemplateEngine\TemplateEngineFactory;
use Macrominds\Rendering\TemplateEngine\Twig\TwigTemplateEngineFactory;
use Macrominds\YamlParser\YamlParserFactory;
use Tests\TestCase;

class DefaultConfigTest extends TestCase
{
    /**
     * @test
     * @dataProvider provideExpectedDefaultConfigField
     */
    public function it_provides_the_expected_defaults($key, $value)
    {
        $defaultConfig = $this->createDefaultConfigInstance()->get();
        $this->assertEquals($value($this->getContainer()[Env::class]), $defaultConfig[$key]);
    }

    public function provideExpectedDefaultConfigField(): array
    {
        $defaultConfig = [
            CommonConfig::KEY_APP_ENV => function (Env $env) {
                return $env->get(CommonConfig::KEY_APP_ENV, 'production');
            },
            ContentPathProvider::CONFIG_KEY_PATH_CONTENT => function () {
                return $this->getProjectRoot('/content');
            },
            TemplateEngineFactory::CONFIG_KEY_PATH_TEMPLATE => function () {
                return $this->getProjectRoot('/templates');
            },
            TemplateEngineFactory::CONFIG_KEY_PATH_CACHE => function () {
                return $this->getProjectRoot('/cache');
            },
            LoggerFactory::CONFIG_KEY_PATH_LOGGING          => function () {
                return $this->getProjectRoot('/log/mwf.log');
            },

            CommonConfig::KEY_SUFFIX_CONTENT_FILE           => 'yml.md',
            CommonConfig::KEY_DEFAULT_RESPONSE_TYPE         => 'html',

            YamlParserFactory::CONFIG_KEY_VARIABLES => [
                'template' => 'standard',
            ],
        ];

        return $this->transformToDataProviderWithFactoryFunctions($defaultConfig);
    }

    private function transformToDataProviderWithFactoryFunctions(array $defaultConfig)
    {
        $keys = array_keys($defaultConfig);

        return array_combine(
            $keys,
            array_map(
                [$this, 'transformToKeyAndCallablePair'],
                $keys,
                $defaultConfig
            )
        );
    }

    private function transformToKeyAndCallablePair($key, $item)
    {
        return [
            $key,
            function (Env $env) use ($item) {
                return is_callable($item) ? $item($env) : $item;
            },
        ];
    }

    /**
     * @test
     */
    public function it_allows_you_to_override_defaults()
    {
        $this->assertEquals(
            $this->getContainer()[Env::class]->get(CommonConfig::KEY_APP_ENV, 'production'),
            $this->createDefaultConfigInstance()->get()[CommonConfig::KEY_APP_ENV]
        );
        $overridden = $this->createDefaultConfigInstance()->merge([CommonConfig::KEY_APP_ENV => 'overridden']);
        $this->assertEquals('overridden', $overridden[CommonConfig::KEY_APP_ENV]);
    }

    /**
     * @test
     */
    public function it_allows_you_to_add_fields()
    {
        $this->assertFalse(isset($this->createDefaultConfigInstance()->get()[TwigTemplateEngineFactory::CONFIG_KEY_FUNCTIONS]));
        $configWithAddedFields = $this->createDefaultConfigInstance()->merge([
                TwigTemplateEngineFactory::CONFIG_KEY_FUNCTIONS => [
                    'css' => function (string $name): string {
                        return "/css/{$name}";
                    },
                ],
        ]);
        $this->assertTrue(isset($configWithAddedFields[TwigTemplateEngineFactory::CONFIG_KEY_FUNCTIONS]));
    }

    /**
     * @test
     */
    public function it_deals_with_nested_settings()
    {
        $this->assertTemplateVariableIsSetToStandard($this->createDefaultConfigInstance()->get());
        $this->assertTemplateVariableIsSetToStandard($this->createDefaultConfigInstance()->merge([
            'variables' => [
                'var'      => [
                    'locale' => 'en',
                ],
            ],
        ]));
    }

    private function assertTemplateVariableIsSetToStandard(array $config)
    {
        $this->assertEquals(
            'standard',
            $config[YamlParserFactory::CONFIG_KEY_VARIABLES]['template']
        );
    }

    private function createDefaultConfigInstance(): DefaultConfig
    {
        return new DefaultConfig($this->getContainer());
    }
}
