<?php

namespace Tests\Unit\Parser;

use Tests\TestCase;

/**
 * @group yaml
 * @group parser
 */
class ValidYamlParserTest extends TestCase
{
    /**
     * @test
     * @dataProvider provideValidYamlBlock
     */
    public function it_parses_a_YAML_as_expected(string $yamlBlock, array $expected)
    {
        $this->assertEquals($expected, (new \Macrominds\YamlParser\YamlParser())->parse($yamlBlock));
    }

    public function provideValidYamlBlock()
    {
        return [
            'simple' => [
<<< YAML
---
title: my title
---
YAML,
                [
                    'title' => 'my title',
                ],
             ],
            'multiline-strings' => [
<<< YAML
---
preserving-newlines: |
  hello
  world
# comment
folding-newlines: >
  hello
  world
---
YAML,
                [
                    'preserving-newlines' => "hello\nworld\n",
                    'folding-newlines' => "hello world\n",
                ],
            ],
            'data-types' => [
<<< YAML
---
# Example taken from https://en.wikipedia.org/wiki/YAML
a: 123                     # an integer
b: "123"                   # a string, disambiguated by quotes
c: 123.0                   # a float
d: !!float 123             # also a float via explicit data type prefixed by (!!)
e: !!str 123               # a string, disambiguated by explicit type
f: !!str Yes               # a string via explicit type
g: Yes                     # a boolean True (yaml1.1), string "Yes" (yaml1.2)
h: Yes we have No bananas  # a string, "Yes" and "No" disambiguated by context.
# More examples
i: true                    # boolean true
j: false                   # boolean false
---
YAML,
                [
                    'a' => 123,
                    'b' => '123',
                    'c' => 123.0,
                    'd' => 123.0,
                    'e' => '123',
                    'f' => 'Yes',
                    'g' => 'Yes',
                    'h' => 'Yes we have No bananas',
                    'i' => true,
                    'j' => false,
                ],
            ],
            'lists' => [
<<< YAML
---
one-per-line:
    - ItemA
    - ItemB
    - ItemC
inline: [a, b, c, d]
---
YAML,
                [
                    'one-per-line' => [
                        'ItemA',
                        'ItemB',
                        'ItemC',
                    ],
                    'inline' => [
                        'a',
                        'b',
                        'c',
                        'd',
                    ],
                ],
            ],
            'objects' => [
<<< YAML
---
# Examples taken from https://en.wikipedia.org/wiki/YAML and modified slightly
smiths:
    - {name: John Smith, age: 33}
    - name: Mary Smith
      age: 27
    # sequences as keys seem not to be supported
    # - [name, age]: [Rae Smith, 4]
genders:
    men: [John Smith, Bill Jones]
    women:
      - Mary Smith
      - Susan Williams
---
YAML,
                [
                    'smiths' => [
                        [
                            'name' => 'John Smith',
                            'age' => 33,
                        ],
                        [
                            'name' => 'Mary Smith',
                            'age' => 27,
                        ],
                    ],
                    'genders' => [
                        'men' => [
                            'John Smith',
                            'Bill Jones',
                        ],
                        'women' => [
                            'Mary Smith',
                            'Susan Williams',
                        ],
                    ],
                ],
            ],
            'advanced-concepts' => [
<<< YAML
---
# Examples taken from https://en.wikipedia.org/wiki/YAML
- step: &id001                  # defines anchor label &id001
    instrument:      Lasik 2000
    pulseEnergy:     5.4
    pulseDuration:   12
    repetition:      1000
    spotSize:        1mm

- step: &id002
    instrument:      Lasik 2000
    pulseEnergy:     5.0
    pulseDuration:   10
    repetition:      500
    spotSize:        2mm
# refers to the first step (with anchor &id001)
- step: *id001
- step: *id002
# refers to the second step
- step: *id002
---
YAML,
                [
                    [
                        'step' => [
                            'instrument' => 'Lasik 2000',
                            'pulseEnergy' => 5.4,
                            'pulseDuration' => 12,
                            'repetition' => 1000,
                            'spotSize' => '1mm',
                        ],
                    ],
                    [
                        'step' => [
                            'instrument' => 'Lasik 2000',
                            'pulseEnergy' => 5.0,
                            'pulseDuration' => 10,
                            'repetition' => 500,
                            'spotSize' => '2mm',
                        ],
                    ],
                    [
                        'step' => [
                            'instrument' => 'Lasik 2000',
                            'pulseEnergy' => 5.4,
                            'pulseDuration' => 12,
                            'repetition' => 1000,
                            'spotSize' => '1mm',
                        ],
                    ],
                    [
                        'step' => [
                            'instrument' => 'Lasik 2000',
                            'pulseEnergy' => 5.0,
                            'pulseDuration' => 10,
                            'repetition' => 500,
                            'spotSize' => '2mm',
                        ],
                    ],
                    [
                        'step' => [
                            'instrument' => 'Lasik 2000',
                            'pulseEnergy' => 5.0,
                            'pulseDuration' => 10,
                            'repetition' => 500,
                            'spotSize' => '2mm',
                        ],
                    ],
                ],
            ],
            'inline-list-on-root-level' => [
                <<< YAML
---
# Placing this on root level has thrown a ParseException before symfony/yaml 4.4 but is fixed now
[ItemA, ItemB]
---
YAML,
                [
                    'ItemA',
                    'ItemB',
                ],
            ],
            'inline-object-on-root-level' => [
                <<< YAML
---
# Placing this on root level has thrown a ParseException before symfony/yaml 4.4 but is fixed now
{name: ItemA, age: 8}
---
YAML,
                [
                    'name' => 'ItemA',
                    'age' => 8,
                ],
            ],
            'sequence-keys' => [
                <<< YAML
---
# has been fixed in latest symfony yaml versions
- [name, age]: [Rae Smith, 4]   # sequences as keys seems to be supported.. however, the values are not, as it seems
---
YAML,
                [
                    [
                        'name', 'age'
                    ],
                ],
            ],
        ];
    }
}
