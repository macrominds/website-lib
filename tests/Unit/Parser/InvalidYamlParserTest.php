<?php

namespace Tests\Unit\Parser;

use Macrominds\YamlParser\YamlParser;
use Symfony\Component\Yaml\Exception\ParseException;
use Tests\TestCase;

/**
 * @group yaml
 * @group parser
 * @group error-handling
 */
class InvalidYamlParserTest extends TestCase
{
    /**
     * @test
     * @dataProvider provideValidYamlBlock
     */
    public function it_throws_appropriate_exceptions_for_invalid_yaml(string $yamlBlock)
    {
        $this->expectException(ParseException::class);
        (new YamlParser())->parse($yamlBlock);
    }

    public function provideValidYamlBlock()
    {
        return [
            'references-with-whitespace' => [
<<< YAML
---
# seems to be a symfony yaml limitation
- step: &id001
- step: *id001    # symfony yaml cannot handle whitespace after reference
---
YAML
             ],
            'switch-from-mapping-to-sequence' => [
<<< YAML
---
item: test
- item1
- item2
---
YAML
            ],
            'switch-from-sequence-to-mapping' => [
<<< YAML
---
- item1
- item2
item: test
---
YAML
            ],
        ];
    }
}
