<?php

namespace Tests\Unit\Parser;

use Macrominds\BasicServiceProvider;
use Macrominds\Config\InvalidConfigurationException;
use Macrominds\Services\Container;
use Macrominds\YamlParser\YamlParser;
use Macrominds\YamlParser\YamlParserFactory;
use Pimple\Container as PimpleContainer;
use Tests\TestCase;

class YamlParserFactoryTest extends TestCase
{
    /**
     * @test
     *
     * @throws InvalidConfigurationException
     */
    public function it_creates_the_appropriate_object_for_a_valid_config()
    {
        $factory = new YamlParserFactory(
            $this->containerForConfiguration($this->getValidConfiguration())
        );

        $yamlParser = $factory->createYamlParser();
        $this->assertInstanceOf(YamlParser::class, $yamlParser);
    }

    private function getValidConfiguration(): array
    {
        return [
            YamlParserFactory::CONFIG_KEY_VARIABLES => [
                'template' => 'standard',
            ],
        ];
    }

    /**
     * @test
     * @dataProvider provideInvalidConfiguration
     *
     * @throws InvalidConfigurationException
     */
    public function it_throws_appropriate_exceptions_in_case_of_missing_configurations($invalidConfigurationFactory, $expectedMessageMatch)
    {
        $factory = new YamlParserFactory(
            $this->containerForConfiguration($invalidConfigurationFactory())
        );
        $this->expectException(InvalidConfigurationException::class);
        $this->expectExceptionMessageMatches($expectedMessageMatch);
        $factory->createYamlParser();
    }

    public function provideInvalidConfiguration(): array
    {
        return [
            'missing variables' => [
                function (): array {
                    return $this->validConfigurationExcept(YamlParserFactory::CONFIG_KEY_VARIABLES);
                },
                '/Configuration \''.YamlParserFactory::CONFIG_KEY_VARIABLES.'\' missing or invalid/',
            ],
        ];
    }

    private function validConfigurationExcept(string $key): array
    {
        $valid = $this->getValidConfiguration();
        unset($valid[$key]);

        return $valid;
    }

    private function containerForConfiguration(array $configuration): Container
    {
        return (new Container(new PimpleContainer()))
            ->registerServiceProvider(new BasicServiceProvider($configuration));
    }
}
