<?php

namespace Tests\Unit\Routing;

use Macrominds\Config\InvalidConfigurationException;
use Macrominds\ContentAccess\ContentAccess;
use Macrominds\ContentAccess\ContentPathDoesNotExistException;
use Macrominds\Request\ParsedRequest;
use Macrominds\Routing\ResourceRouter;
use Symfony\Component\HttpFoundation\Request;
use Tests\TestCase;

/**
 * @group request
 */
class ResourceRouterTest extends TestCase
{
    /**
     * @test
     *
     * @throws ContentPathDoesNotExistException
     * @throws InvalidConfigurationException
     */
    public function it_finds_the_path_for_an_existing_resource()
    {
        $this->setupAssertion(
            '/general',
            '/existing.html',
            function (ContentAccess $contentAccess, string $path) {
                $this->assertEquals($contentAccess->findExisting('/existing.yml.md'), $path);
            });
    }

    /**
     * @throws ContentPathDoesNotExistException
     * @throws InvalidConfigurationException
     */
    private function setupAssertion(string $contentPath, string $requestedPath, callable $then)
    {
        $contentAccess = $this->getContentAccessForRelativePath($contentPath);
        $path = (new ResourceRouter($contentAccess, 'yml.md'))
            ->route(
                new ParsedRequest(Request::create($requestedPath))
            );
        $then($contentAccess, $path);
    }

    /**
     * @test
     *
     * @throws ContentPathDoesNotExistException
     * @throws InvalidConfigurationException
     */
    public function it_throws_a_ContentPathDoesNotExistException_for_a_non_existing_resource()
    {
        $this->expectException(ContentPathDoesNotExistException::class);
        $this->setupAssertion(
            '/general',
            '/non-existing.html',
            function () {
                $this->fail('/404.yml.md');
            });
    }

    /**
     * @test
     *
     * @throws ContentPathDoesNotExistException
     * @throws InvalidConfigurationException
     */
    public function it_throws_an_exception_if_theres_no_404_but_a_500_error_page()
    {
        $this->expectException(ContentPathDoesNotExistException::class);
        $this->setupAssertion(
            '/no-404-but-500',
            '/non-existing.html',
            function () {
                $this->fail();
            });
    }

    /**
     * @test
     *
     * @throws ContentPathDoesNotExistException
     * @throws InvalidConfigurationException
     */
    public function it_throws_an_exception_if_neither_content_nor_404_nor_500_path_is_found()
    {
        $this->expectException(ContentPathDoesNotExistException::class);
        $this->setupAssertion(
            '/empty',
            '/non-existing.html',
            function () {
                $this->fail();
            });
    }
}
