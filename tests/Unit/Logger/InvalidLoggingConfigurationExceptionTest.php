<?php

namespace Tests\Unit\Logger;

use Macrominds\Config\Config;
use Macrominds\Config\InvalidConfigurationException;
use Macrominds\Logger\LoggerFactory;
use Monolog\Logger;
use Tests\TestCase;

class InvalidLoggingConfigurationExceptionTest extends TestCase
{
    /**
     * @test
     *
     * @throws InvalidConfigurationException
     */
    public function it_is_thrown_when_the_logging_path_is_not_configured()
    {
        $this->expectException(InvalidConfigurationException::class);
        $this->getLoggerWithoutConfiguredLoggingPath();
    }

    /**
     * @throws InvalidConfigurationException
     */
    public function getLoggerWithoutConfiguredLoggingPath(): Logger
    {
        return (new LoggerFactory(new Config()))->createLogger('mwf');
    }

    /**
     * @test
     * @dataProvider provideInvalidLoggingPath
     *
     * @throws InvalidConfigurationException
     */
    public function it_is_thrown_when_the_configured_path_is_syntactically_invalid($invalidPath)
    {
        $this->expectException(InvalidConfigurationException::class);
        $this->getLoggerWithLoggingPath($invalidPath);
    }

    public function provideInvalidLoggingPath()
    {
        return [
            'integer' => [1],
            'array' => [[]],
            'url-in-array' => [['bogus:/url']],
            'null' => [null],
        ];
    }

    /**
     * @throws InvalidConfigurationException
     */
    public function getLoggerWithLoggingPath($path): Logger
    {
        return (new LoggerFactory(new Config([
            LoggerFactory::CONFIG_KEY_PATH_LOGGING => $path,
        ])))->createLogger('mwf');
    }

    /**
     * @test
     */
    public function it_contains_information_about_the_missing_configuration()
    {
        try {
            $this->getLoggerWithoutConfiguredLoggingPath();
            $this->fail('expected exception to be thrown');
        } catch (InvalidConfigurationException $e) {
            $this->assertEquals(LoggerFactory::CONFIG_KEY_PATH_LOGGING, $e->getInvalidConfigurationKey());
        }
    }

    /**
     * @test
     * @dataProvider provideInvalidLoggingPath
     */
    public function it_contains_information_about_the_invalid_configuration($path)
    {
        try {
            $this->getLoggerWithLoggingPath($path);
            $this->fail('expected exception to be thrown');
        } catch (InvalidConfigurationException $e) {
            $this->assertEquals(LoggerFactory::CONFIG_KEY_PATH_LOGGING, $e->getInvalidConfigurationKey());
        }
    }
}
