<?php

namespace Tests\Unit\Logger;

use Macrominds\Config\Config;
use Macrominds\Config\InvalidConfigurationException;
use Macrominds\Logger\LoggerFactory;
use Monolog\Logger;
use Tests\TestCase;

class LoggerFactoryTest extends TestCase
{
    /**
     * @test
     *
     * @throws InvalidConfigurationException
     */
    public function it_retrieves_a_valid_logger_for_a_valid_configuration()
    {
        $loggerFactory = new LoggerFactory(new Config([
            LoggerFactory::CONFIG_KEY_PATH_LOGGING => 'php://memory',
        ]));
        $logger = $loggerFactory->createLogger('mwf');
        $this->assertNotNull($logger);
        $this->assertInstanceOf(Logger::class, $logger);
    }
}
