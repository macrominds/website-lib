<?php

namespace Tests\Unit\Request\Rewrite;

use Macrominds\Request\ParsedRequest;
use Macrominds\Request\Rewrite\RootSlashToIndexRewriter;
use Symfony\Component\HttpFoundation\Request;
use Tests\TestCase;

class RootSlashToIndexRewriterTest extends TestCase
{
    /**
     * @test
     */
    public function it_rewrites_slash_to_index()
    {
        $request = new ParsedRequest(Request::create('/'));
        $rewrittenRequest = (new RootSlashToIndexRewriter())
            ->rewriteRequestPath($request);

        $this->assertEquals('/index.html', $rewrittenRequest->getPathInfo());
    }
}
