<?php

namespace Tests\Unit\Request;

use Macrominds\Request\ParsedRequest;
use Symfony\Component\HttpFoundation\Request;
use Tests\TestCase;

class ParsedRequestTest extends TestCase
{
    /**
     * @test
     */
    public function it_provides_a_pathinfo()
    {
        $parsedRequest = new ParsedRequest(Request::create('/index.html'));
        $this->assertEquals('/index.html', $parsedRequest->getPathInfo());
    }

    /**
     * @test
     */
    public function it_provides_a_suffix()
    {
        $parsedRequest = new ParsedRequest(Request::create('/index.html'));
        $this->assertEquals('html', $parsedRequest->getSuffix('unused-fallback-suffix'));
    }

    /**
     * @test
     */
    public function it_uses_a_fallback_if_suffix_is_missing()
    {
        $parsedRequest = new ParsedRequest(Request::create('/index'));
        $this->assertEquals('fallback-suffix', $parsedRequest->getSuffix('fallback-suffix'));
    }
}
