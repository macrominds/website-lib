<?php

namespace Tests\Unit\Config;

use Macrominds\Config\CommonConfig;
use Macrominds\Config\Config;
use Tests\TestCase;

/**
 * @group config
 */
class CommonConfigTest extends TestCase
{
    /**
     * @test
     */
    public function it_retrieves_the_target_suffix()
    {
        $config = new CommonConfig(new Config([
            CommonConfig::KEY_SUFFIX_CONTENT_FILE   => 'yml.md',
        ]));
        $this->assertEquals('yml.md', $config->getContentFileSuffix());
    }

    /**
     * @test
     */
    public function it_returns_a_common_fallback_if_the_target_suffix_is_not_configured()
    {
        $config = new CommonConfig(new Config([
            'not-configured: content-file-type' => 'not-configured',
        ]));
        $this->assertEquals('yml.md', $config->getContentFileSuffix());
    }

    /**
     * @test
     */
    public function it_retrieves_the_default_response_type()
    {
        $config = new CommonConfig(new Config([
            CommonConfig::KEY_DEFAULT_RESPONSE_TYPE => 'html',
        ]));
        $this->assertEquals('html', $config->getDefaultResponseType());
    }

    /**
     * @test
     */
    public function it_returns_a_common_fallback_if_the_default_suffix_is_not_configured()
    {
        $config = new CommonConfig(new Config([
            'not-configured: default-response-type' => 'not-configured',
        ]));
        $this->assertEquals('html', $config->getDefaultResponseType());
    }
}
