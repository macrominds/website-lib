<?php

namespace Tests\Unit;

use Macrominds\App\DefaultProjectPathProvider;
use Macrominds\App\ProjectPathProvider;
use Macrominds\BasicServiceProvider;
use Macrominds\Config\CommonConfig;
use Macrominds\Config\Config;
use Macrominds\Config\Env;
use Macrominds\ContentAccess\ContentAccess;
use Macrominds\ContentAccess\ContentPathProvider;
use Macrominds\Lifecycle\AbstractLifecycle;
use Macrominds\Lifecycle\ResponseFactory;
use Macrominds\Logger\LoggerFactory;
use Macrominds\MissingProjectPathProviderException;
use Macrominds\Rendering\TemplateEngine\TemplateEngineFactory;
use Macrominds\Request\Rewrite\RequestRewriterStrategy;
use Macrominds\Resource\ErrorResourceFactory;
use Macrominds\Resource\ResourceLocator;
use Macrominds\Routing\ResourceRouter;
use Macrominds\Services\Container;
use Macrominds\YamlParser\YamlParserFactory;
use Symfony\Component\HttpFoundation\Request;
use Tests\TestCase;

/**
 * @group services
 */
class BasicServiceProviderTest extends TestCase
{
    // TODO rethink this test
    // It should be the App's responsibility to verify that all required services are provided, shouldn't it?

    /**
     * @test
     */
    public function it_preconfigures_all_required_parameters()
    {
        foreach ($this->getRequiredParameterKeys() as $requiredParameterKey) {
            $this->assertArrayHasKey(
                $requiredParameterKey,
                $this->createAppServiceProvider()->getParameters()
            );
        }
    }

    private function getRequiredParameterKeys(): array
    {
        return [
            'config',
        ];
    }

    /**
     * @test
     */
    public function it_preconfigures_all_required_services()
    {
        foreach ($this->getRequiredServiceKeys() as $requiredServiceKey) {
            $this->assertArrayHasKey(
                $requiredServiceKey,
                $this->createAppServiceProvider()->getServices($this->getContainer())
            );
        }
    }

    private function getRequiredServiceKeys(): array
    {
        return array_merge(
            $this->getRequiredSingletonServiceKeys(),
            $this->getRequiredFactoryObjectServiceKeys()
        );
    }

    private function getRequiredSingletonServiceKeys(): array
    {
        return [
            ProjectPathProvider::class,
            Config::class,
            Env::class,
            CommonConfig::class,
            LoggerFactory::class,
            ResponseFactory::class,
            RequestRewriterStrategy::class,
            ContentAccess::class,
            ContentPathProvider::class,
            ResourceRouter::class,
            ResourceLocator::class,
            ErrorResourceFactory::class,
            AbstractLifecycle::class,
        ];
    }

    private function getRequiredFactoryObjectServiceKeys(): array
    {
        return [
            Request::class,
        ];
    }

    /**
     * @test
     */
    public function it_registers_the_required_services_and_configs_with_the_container()
    {
        $requiredKeys = array_merge($this->getRequiredParameterKeys(), $this->getRequiredServiceKeys());
        $container = $this->getContainer();
        $container->registerServiceProvider(new BasicServiceProvider());
        foreach ($requiredKeys as $requiredKey) {
            $this->assertArrayHasKey($requiredKey, $container,
                "Expected {$requiredKey} to be contained within AppServiceProvider services.");
        }
    }

    /**
     * @test
     */
    public function it_registers_functions_correctly()
    {
        $serviceProvider = new class() extends BasicServiceProvider {
            public function getParameters(): array
            {
                return [
                    'a_function' => function () {
                        return 'is registered correctly';
                    },
                ];
            }

            public function getServices(Container $c): array
            {
                return [
                ];
            }
        };
        $container = $this->getContainer()->registerServiceProvider($serviceProvider);
        $this->assertEquals('is registered correctly', $container['a_function']());
    }

    /**
     * @test
     */
    public function it_provides_the_services_as_singletons_or_factory_objects_appropriately()
    {
        $container = $this->getContainer()->registerServiceProvider(new BasicServiceProvider([
            ContentPathProvider::CONFIG_KEY_PATH_CONTENT => $this->getContentPath('/general'),
            TemplateEngineFactory::CONFIG_KEY_PATH_CACHE => $this->getCachePath(),
            TemplateEngineFactory::CONFIG_KEY_PATH_TEMPLATE => $this->getTemplatePath('/general'),
            YamlParserFactory::CONFIG_KEY_VARIABLES => [
                'template' => 'standard',
            ],
        ]));
        $container->singleton(
            ProjectPathProvider::class,
            function () {
                return new DefaultProjectPathProvider($this->getProjectRoot());
            }
        );
        foreach ($this->getRequiredSingletonServiceKeys() as $singleton) {
            $firstFetch = $container->resolve($singleton);
            $secondFetch = $container->resolve($singleton);
            $this->assertSame($firstFetch, $secondFetch);
        }

        foreach ($this->getRequiredFactoryObjectServiceKeys() as $factoryObject) {
            $firstFetch = $container->resolve($factoryObject);
            $secondFetch = $container->resolve($factoryObject);
            $this->assertNotSame($firstFetch, $secondFetch);
        }
    }

    /**
     * @test
     */
    public function it_signals_a_missing_ProjectPathProvider()
    {
        $container = $this->getContainer()->registerServiceProvider(new BasicServiceProvider());
        $this->expectException(MissingProjectPathProviderException::class);
        $container->resolve(ProjectPathProvider::class);
    }
}
