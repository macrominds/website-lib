<?php

namespace Tests\Unit\Page;

use Macrominds\Page\Page;
use Tests\TestCase;

/**
 * @group processing
 */
class PageTest extends TestCase
{
    /**
     * @test
     */
    public function it_stores_the_content()
    {
        $page = new Page('content', []);
        $this->assertEquals('content', $page->getContent());
    }

    /**
     * @test
     */
    public function it_stores_the_data()
    {
        $data = ['a' => 1, 'b' => 'b'];
        $page = new Page('content', $data);
        $this->assertEquals($data, $page->getData());
    }

    /**
     * @test
     */
    public function it_provides_easy_access_to_data()
    {
        $data = ['a' => 1, 'b' => 'b'];
        $page = new Page('content', $data);
        $this->assertEquals(1, $page->data('a'));
    }

    /**
     * @test
     */
    public function it_returns_null_if_data_is_not_set()
    {
        $data = ['a' => 1, 'b' => 'b'];
        $page = new Page('content', $data);
        $this->assertNull($page->data('c'));
    }

    /**
     * @test
     */
    public function it_returns_the_fallback_if_data_is_not_set()
    {
        $data = ['a' => 1, 'b' => 'b'];
        $page = new Page('content', $data);
        $this->assertEquals('fallback', $page->data('c', 'fallback'));
    }

    /**
     * @test
     */
    public function it_can_access_nested_values_using_dot_notation()
    {
        $data = [
            'nested' => [
                'access' => [
                    'using' => 'dot.notation',
                ],
            ],
        ];
        $page = new Page('content', $data);

        $this->assertEquals('dot.notation', $page->data('nested.access.using', 'fallback'));

        $this->assertEquals('fallback', $page->data('non-existing.nested.value', 'fallback'));
    }
}
