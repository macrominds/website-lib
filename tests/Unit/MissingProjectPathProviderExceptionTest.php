<?php

namespace Tests\Unit;

use Macrominds\MissingProjectPathProviderException;
use Tests\TestCase;

class MissingProjectPathProviderExceptionTest extends TestCase
{
    /**
     * @var string
     */
    private $message;
    /**
     * @var MissingProjectPathProviderException
     */
    private $exception;

    /**
     * @before
     */
    public function setupMessageAndException()
    {
        $this->message = 'A message.';
        $this->exception = new MissingProjectPathProviderException($this->message);
    }

    /**
     * @test
     */
    public function it_contains_the_message()
    {
        $this->assertStringContainsString(
            $this->message,
            $this->exception->getMessage()
        );
    }

    /**
     * @test
     */
    public function it_contains_instructions_about_how_to_provide_the_provider()
    {
        $this->assertStringContainsString(
            MissingProjectPathProviderException::getInstruction(),
            $this->exception->getMessage()
        );
    }
}
