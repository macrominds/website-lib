<?php

namespace Tests\Unit\ContentAccess;

use Macrominds\Config\Config;
use Macrominds\Config\InvalidConfigurationException;
use Macrominds\ContentAccess\ContentAccess;
use Macrominds\ContentAccess\ContentPathDoesNotExistException;
use Macrominds\ContentAccess\ContentPathProvider;
use Tests\TestCase;

/**
 * @group content
 */
class ContentAccessTest extends TestCase
{
    use ProvideContentPath;

    /**
     * @test
     * @dataProvider provideValidPathConfig
     *
     * @throws InvalidConfigurationException
     */
    public function it_creates_a_content_access_for_a_valid_path($validPath)
    {
        $contentAccess = new ContentAccess($this->getContentPathProviderForAbsolutePath($validPath));
        $this->assertNotNull($contentAccess);
        $this->assertInstanceOf(ContentAccess::class, $contentAccess);
    }

    /**
     * @test
     *
     * @throws InvalidConfigurationException
     * @dataProvider provideInvalidPath
     */
    public function it_throws_an_exception_when_created_for_an_invalid_path($invalidPath)
    {
        $config = $this->getContainer()->resolve(Config::class);

        // need to go the long winded way, because we're using null and
        // helper method type hinting prevents us from doing so
        $config->set(
            ContentPathProvider::CONFIG_KEY_PATH_CONTENT, $invalidPath
        );
        $contentPathProvider = new ContentPathProvider($config);

        $this->expectException(InvalidConfigurationException::class);
        new ContentAccess($contentPathProvider);
    }

    /**
     * @test
     *
     * @throws InvalidConfigurationException
     */
    public function it_stores_the_content_path()
    {
        $expectedPath = $this->getContentPath('/my-content-path');
        $contentAccess = $this->getContentAccessForAbsolutePath($expectedPath);
        $this->assertEquals($expectedPath, $contentAccess->getPath());
    }

    /**
     * @test
     * @dataProvider provideExistingUnmappedPaths
     *
     * @throws ContentPathDoesNotExistException
     * @throws InvalidConfigurationException
     */
    public function it_finds_existing_source_paths($relativePath)
    {
        $contentPath = $this->getContentPath('/general');
        $contentAccess = $this->getContentAccessForAbsolutePath($contentPath);

        $path = $contentAccess->findExisting($relativePath);
        $this->assertEquals(realpath("{$contentPath}/{$relativePath}"), $path);
    }

    public function provideExistingUnmappedPaths()
    {
        return [
            'existing.yml.md' => [
                'existing.yml.md',
            ],
            'subdir/sub.yml.md' => [
                'existing.yml.md',
            ],
        ];
    }

    /**
     * @test
     * @dataProvider provideSourceAndMappedPath
     *
     * @throws ContentPathDoesNotExistException
     * @throws InvalidConfigurationException
     */
    public function it_finds_existing_mapped_paths($source, $expected, $targetSuffix)
    {
        $contentPath = $this->getContentPath('/general');
        $contentAccess = $this->getContentAccessForAbsolutePath($contentPath);
        $path = $contentAccess->findExistingMappedPath($source, $targetSuffix);
        $this->assertEquals("{$contentPath}/{$expected}", $path);
    }

    public function provideSourceAndMappedPath()
    {
        return [
            'root-source' => [
                'source' => 'existing.html',
                'expected' => 'existing.yml.md',
                'target-suffix' => 'yml.md',
            ],
            'sub-source' => [
                'source' => 'subdir/sub.yml.md',
                'expected' => 'subdir/sub.yml.md',
                'target-suffix' => 'yml.md',
            ],
        ];
    }

    /**
     * @test
     * @dataProvider provideNonexistingContentPaths
     *
     * @throws ContentPathDoesNotExistException
     * @throws InvalidConfigurationException
     */
    public function it_throws_an_appropriate_exception_when_the_content_path_could_not_be_found($filename)
    {
        $this->expectException(ContentPathDoesNotExistException::class);
        $this->getContentAccessForAbsolutePath('/general')->findExisting($filename);
    }

    public function provideNonexistingContentPaths()
    {
        return [
            'non-existing.html' => [
                'non-existing.html',
            ],
            'subdir/sub.xml.md' => [
                'subdir/sub.xml.md',
            ],
            '' => [
                // is the root content directory and thus effectively the same test as 'a directory, not a file' below
                '',
            ],
            'a directory, not a file' => [
                'subdir',
            ],
            ' existing.yml.md' => [
                ' existing.yml.md',
            ],
            'existing.yml.md ' => [
                'existing.yml.md ',
            ],
        ];
    }

    /**
     * @test
     * @dataProvider provideRealPathWithBelowContentPathExpectation
     *
     * @throws InvalidConfigurationException
     */
    public function it_reports_if_a_real_path_is_inside_content_path($realpathFactory, $expected)
    {
        $contentAccess = $this->getContentAccessForRelativePath('/general');

        $this->assertEquals(
            $expected,
            $contentAccess->isBelowContentPath($realpathFactory())
        );

        $otherPath = $this->getContentPath('/other');
        $this->assertFalse(
            $contentAccess
                ->isBelowContentPath(
                    realpath("{$otherPath}/existing.yml.md")
                )
        );
    }

    public function provideRealPathWithBelowContentPathExpectation()
    {
        return [
            'simple-below-path' => [
                function () {
                    $contentPath = $this->getContentPath('/general');

                    return realpath("{$contentPath}/existing.yml.md");
                },
                true,
            ],
            'simple-outside-path' => [
                function () {
                    $otherPath = $this->getContentPath('/other');

                    return realpath("{$otherPath}/existing.yml.md");
                },
                false,
            ],
            'complex-outside-path' => [
                function () {
                    $otherPath = $this->getContentPath('/general/../other');

                    return realpath("{$otherPath}/existing.yml.md");
                },
                false,
            ],
            'complex-below-path' => [
                function () {
                    $otherPath = $this->getContentPath('/other/../general');

                    return realpath("{$otherPath}/existing.yml.md");
                },
                true,
            ],
        ];
    }
}
