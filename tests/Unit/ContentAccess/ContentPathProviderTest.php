<?php

namespace Tests\Unit\ContentAccess;

use Macrominds\Config\Config;
use Macrominds\Config\InvalidConfigurationException;
use Macrominds\ContentAccess\ContentPathProvider;
use Tests\TestCase;

class ContentPathProviderTest extends TestCase
{
    use ProvideContentPath;

    /**
     * @test
     *
     * @throws InvalidConfigurationException
     * @dataProvider provideValidPathConfig
     */
    public function it_retrieves_a_valid_path_config($validPath)
    {
        $pathProvider = new ContentPathProvider(new Config([
            ContentPathProvider::CONFIG_KEY_PATH_CONTENT => $validPath,
        ]));
        $this->assertEquals($validPath, $pathProvider->getValidatedContentPath());
    }

    /**
     * @test
     *
     * @throws InvalidConfigurationException
     * @dataProvider provideInvalidPath
     */
    public function it_throws_an_exception_if_the_path_config_is_not_valid($invalidPath)
    {
        $pathProvider = new ContentPathProvider(new Config([
            ContentPathProvider::CONFIG_KEY_PATH_CONTENT => $invalidPath,
        ]));
        $this->assertInvalidPathContentConfig($pathProvider);
    }

    /**
     * @throws InvalidConfigurationException
     */
    private function assertInvalidPathContentConfig(ContentPathProvider $pathProvider)
    {
        $this->expectException(InvalidConfigurationException::class);
        $pathProvider->getValidatedContentPath();
    }

    /**
     * @test
     *
     * @throws InvalidConfigurationException
     */
    public function it_throws_an_exception_if_the_path_config_is_not_set()
    {
        $pathProvider = new ContentPathProvider(new Config([
            // unset content path
        ]));
        $this->assertInvalidPathContentConfig($pathProvider);
    }
}
