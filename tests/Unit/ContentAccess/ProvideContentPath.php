<?php

namespace Tests\Unit\ContentAccess;

trait ProvideContentPath
{
    public function provideValidPathConfig(): array
    {
        return [
            'existing path' => [
                '/general',
            ],
            'not existing but valid path' => [
                '/not-existing',
            ],
            'empty' => [
                '',
            ],
            'only whitespace but valid path' => [
                ' ',
            ],
        ];
    }

    public function provideInvalidPath()
    {
        return [
            'null' => [
                null,
            ],
        ];
    }
}
