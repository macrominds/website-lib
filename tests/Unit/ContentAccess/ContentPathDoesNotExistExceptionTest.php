<?php

namespace Tests\Unit\ContentAccess;

use Macrominds\ContentAccess\ContentPathDoesNotExistException;
use Tests\TestCase;

class ContentPathDoesNotExistExceptionTest extends TestCase
{
    /**
     * @test
     */
    public function it_returns_the_affected_path_and_content_dir()
    {
        $expectedPath = 'path';
        $expectedContentDir = 'content-dir';
        $c = new ContentPathDoesNotExistException($expectedPath, $expectedContentDir);
        $this->assertEquals($expectedPath, $c->getPath());
        $this->assertEquals($expectedContentDir, $c->getContentDir());
    }
}
