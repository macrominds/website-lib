<?php

namespace Tests\Unit;

use Macrominds\ContentAccess\ContentPathDoesNotExistException;
use Macrominds\FrontmatterPageFactory;
use Macrominds\Rendering\MarkdownRenderer;
use Macrominds\Rendering\TemplateEngine\TemplateNotFoundException;
use Macrominds\Resource\ContentResource;
use Macrominds\YamlParser\YamlParser;
use Tests\TestCase;

/**
 * @group processing
 */
class FrontmatterPageFactoryTest extends TestCase
{
    /**
     * @test
     *
     * @throws ContentPathDoesNotExistException
     * @throws TemplateNotFoundException
     */
    public function it_processes_yaml_and_markdown()
    {
        $page = (new FrontmatterPageFactory(
            new YamlParser(),
            new MarkdownRenderer()
        ))->createFrom(
            new ContentResource(
                $this->getContentPath('/general').'/index.yml.md',
                'html'
            )
        );
        $rendered = <<< HTML
<h1>This is the index page</h1>
HTML;
        $this->assertEquals([
            [
                'title' => 'index',
            ],
            $rendered,
        ], [$page->getData(), $page->getContent()]);
    }
}
