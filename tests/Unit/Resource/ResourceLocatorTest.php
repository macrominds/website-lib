<?php

namespace Tests\Unit\Resource;

use Macrominds\Config\CommonConfig;
use Macrominds\Config\Config;
use Macrominds\Config\InvalidConfigurationException;
use Macrominds\ContentAccess\ContentPathDoesNotExistException;
use Macrominds\Request\ParsedRequest;
use Macrominds\Resource\ContentResource;
use Macrominds\Resource\ResourceLocator;
use Macrominds\Routing\ResourceRouter;
use Symfony\Component\HttpFoundation\Request;
use Tests\TestCase;

class ResourceLocatorTest extends TestCase
{
    /**
     * @test
     *
     * @throws ContentPathDoesNotExistException
     * @throws InvalidConfigurationException
     */
    public function it_locates_existing_content()
    {
        $relativeContentDir = '/general';
        $resource = $this->locateResource($relativeContentDir, '/index.html');

        $this->assertEquals(
            $this->getContentPath("{$relativeContentDir}/index.yml.md"),
            $resource->getId()
        );
    }

    /**
     * @throws ContentPathDoesNotExistException
     * @throws InvalidConfigurationException
     */
    private function locateResource($relativeContentDir, $requestUri): ContentResource
    {
        $commonConfig = new CommonConfig(new Config());
        $locator = new ResourceLocator(
            new ResourceRouter(
                $this->getContentAccessForRelativePath($relativeContentDir),
                $commonConfig->getContentFileSuffix()
            ),
           $commonConfig
        );

        return $locator->locateResource(new ParsedRequest(Request::create($requestUri)));
    }

    /**
     * @test
     *
     * @throws ContentPathDoesNotExistException
     * @throws InvalidConfigurationException
     */
    public function it_stores_the_requested_path_in_the_resource_context()
    {
        $resource = $this->locateResource('/general', '/index.html');
        $this->assertEquals('/index.html', $resource->getContext()['requested_path']);
    }

    /**
     * @test
     *
     * @throws ContentPathDoesNotExistException
     * @throws InvalidConfigurationException
     */
    public function it_throws_an_exception_if_the_content_cannot_be_found()
    {
        $this->expectException(ContentPathDoesNotExistException::class);
        $this->locateResource('/empty', '/index.html');
    }
}
