<?php

namespace Tests\Unit\Resource;

use Exception;
use Macrominds\Config\CommonConfig;
use Macrominds\Config\Config;
use Macrominds\Config\InvalidConfigurationException;
use Macrominds\ContentAccess\ContentPathDoesNotExistException;
use Macrominds\Resource\ErrorResourceFactory;
use Tests\TestCase;

class ErrorResourceFactoryTest extends TestCase
{
    /**
     * @test
     *
     * @throws ContentPathDoesNotExistException
     * @throws InvalidConfigurationException
     */
    public function it_creates_an_appropriate_resource_for_a_throwable()
    {
        $factory = new ErrorResourceFactory(
            $this->getContentAccessForRelativePath(),
            new CommonConfig(new Config()),
            true
        );
        $resource = $factory->createErrorResource(new Exception('message', 1));
        $this->assertEquals($this->getContentPath('/general/500.yml.md'), $resource->getId());
        $this->assertEquals('html', $resource->getType());
        $actual = $resource->getContext()->toArray();
        $this->assertArrayHasKey('internal', $actual);
        $this->assertArrayHasKey('err', $actual['internal']);
        unset($actual['internal']['err']['file']);
        unset($actual['internal']['err']['line']);
        $this->assertEquals([
            'internal' => [
                'err' => [
                    'no' => 1,
                    'str' => 'message',
                ],
            ],
        ], $actual);
    }

    /**
     * @test
     *
     * @throws ContentPathDoesNotExistException
     * @throws InvalidConfigurationException
     */
    public function it_throws_an_exception_if_the_500_content_is_not_found()
    {
        $factory = new ErrorResourceFactory(
            $this->getContentAccessForRelativePath('/empty'),
            new CommonConfig(new Config()),
            true
        );
        $this->expectException(ContentPathDoesNotExistException::class);
        $factory->createErrorResource(new Exception('message', 1));
    }

    /**
     * @test
     *
     * @throws ContentPathDoesNotExistException
     * @throws InvalidConfigurationException
     */
    public function it_doesnt_expose_the_exception_if_we_are_in_production_mode()
    {
        $factory = new ErrorResourceFactory(
            $this->getContentAccessForRelativePath(),
            new CommonConfig(new Config()),
            false
        );
        $resource = $factory->createErrorResource(new Exception('message', 1));
        $this->assertEquals($this->getContentPath('/general/500.yml.md'), $resource->getId());
        $this->assertEquals('html', $resource->getType());
        $actual = $resource->getContext()->toArray();
        $this->assertArrayHasKey('internal', $actual);
        $this->assertArrayHasKey('err', $actual['internal']);
        unset($actual['internal']['err']['file']);
        unset($actual['internal']['err']['line']);
        $this->assertEquals([
            'internal' => [
                'err' => [
                    'str' => 'An error occurred. See logs.',
                ],
            ],
        ], $actual);
    }
}
