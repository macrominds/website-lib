<?php

namespace Tests\Unit\Resource;

use Macrominds\Rendering\TemplateEngine\Context;
use Macrominds\Resource\ContentResource;
use Tests\TestCase;

/**
 * @group resource
 */
class ContentResourceTest extends TestCase
{
    /**
     * @var ContentResource
     */
    private $resource;

    /**
     * @before
     */
    public function setUp(): void
    {
        $this->resource = new ContentResource('/index.yml.md', 'html');
        parent::setUp();
    }

    /**
     * @test
     */
    public function it_stores_the_id()
    {
        $this->assertEquals('/index.yml.md', $this->resource->getId());
    }

    /**
     * @test
     */
    public function it_stores_the_type()
    {
        $this->assertEquals('html', $this->resource->getType());
    }

    /**
     * @test
     */
    public function it_stores_context()
    {
        $expected = [
            'internal.info' => 'processing instruction',
        ];
        $resource = $this->resource->setContext(new Context($expected));
        $this->assertEquals($expected, $resource->getContext()->toArray());
    }

    /**
     * @test
     * @dataProvider mergeDataProvider
     */
    public function it_merges_context_data($original, $new, $expected)
    {
        $resource = $this->resource->setContext(new Context($original));
        $resource = $resource->mergeContextData($new);

        $this->assertEquals($expected, $resource->getContext()->toArray());
    }

    public function mergeDataProvider(): array
    {
        return [
            'flat merge' => [
                [
                    'content-path' => '/index.html',
                ],
                [
                    'search-phrase' => 'my search',
                ],
                [
                    'content-path' => '/index.html',
                    'search-phrase' => 'my search',
                ],
            ],
            'deep merge' => [
                [
                    'internal' => [
                        'content-type' => 'html',
                    ],
                ],
                [
                    'internal' => [
                        'err' => [
                            'no' => 1,
                            'str' => 'error',
                        ],
                    ],
                ],
                [
                    'internal' => [
                        'content-type' => 'html',
                        'err' => [
                            'no' => 1,
                            'str' => 'error',
                        ],
                    ],
                ],
            ],
            'deep merge with replace' => [
                [
                    'internal' => [
                        'err' => [
                            'no' => 0,
                            'str' => 'old error',
                        ],
                    ],
                ],
                [
                    'internal' => [
                        'err' => [
                            'no' => 1,
                            'str' => 'new error',
                        ],
                    ],
                ],
                [
                    'internal' => [
                        'err' => [
                            'no' => 1,
                            'str' => 'new error',
                        ],
                    ],
                ],
            ],
        ];
    }
}
