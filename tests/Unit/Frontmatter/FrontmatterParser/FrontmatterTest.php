<?php

namespace Tests\Unit\Frontmatter\FrontmatterParser;

use Macrominds\Frontmatter\FrontmatterParser;
use Tests\TestCase;

/**
 * @group parser
 */
class FrontmatterTest extends TestCase
{
    /**
     * @test
     * @dataProvider provideContentAndExpectedFrontmatterAndRemainder
     */
    public function it_extracts_frontmatter($content, $expectedFrontmatter, $expectedRemainder)
    {
        $frontmatterExtractor = new FrontmatterParser();
        [$actual, $remainder] = $frontmatterExtractor->extract($content);
        $this->assertEquals($expectedFrontmatter, $actual);
        $this->assertEquals($expectedRemainder, $remainder);
    }

    public function provideContentAndExpectedFrontmatterAndRemainder()
    {
        $result = [];

        // simple
        $expectedFrontmatter = <<<FRONTMATTER_CONTENT
---
my-arbitrary
{frontmatter}
--syntax--
 ---
...
---

FRONTMATTER_CONTENT;
        $expectedRemainder = "# My markup content\n";

        $result['simple-content'] = [
            "{$expectedFrontmatter}{$expectedRemainder}",
            $expectedFrontmatter,
            $expectedRemainder,
        ];

        // empty
        $expectedFrontmatter = '';
        $expectedRemainder = "# My markup content\n";

        $result['empty-frontmatter'] = [
            "{$expectedFrontmatter}{$expectedRemainder}",
            $expectedFrontmatter,
            $expectedRemainder,
        ];

        // only first block between 3 dashes treated as frontmatter
        $expectedFrontmatter = <<<FRONTMATTER_CONTENT
---
only-first-block-between-3-dashes-treated-as-frontmatter
---

FRONTMATTER_CONTENT;
        $expectedRemainder = <<< MARKDOWN
# Markdown
---
# Multiple sections
---
# Test
MARKDOWN;
        $result['multiple-blocks-of-3-dashes'] = [
            "{$expectedFrontmatter}{$expectedRemainder}",
            $expectedFrontmatter,
            $expectedRemainder,
        ];

        $content = <<<CONTENT
---
no-end-token-given

CONTENT;
        $expectedFrontmatter = '';
        $expectedRemainder = $content;

        $result['no-end-token-given'] = [
            $content,
            $expectedFrontmatter,
            $expectedRemainder,
        ];

        return $result;
    }
}
