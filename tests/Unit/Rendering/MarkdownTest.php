<?php

namespace Tests\Unit\Rendering\Parser;

use Macrominds\Rendering\MarkdownRenderer;
use Tests\TestCase;

/**
 * @group parser
 * @group markdown
 */
class MarkdownTest extends TestCase
{
    /**
     * @test
     * @dataProvider provideSimpleMarkdown
     */
    public function it_parses_a_simple_markdown_block_as_expected(string $markdown, string $expected)
    {
        $this->assertParseMarkdownAsExpected($markdown, $expected);
    }

    public function provideSimpleMarkdown()
    {
        return [
            'simple-markdown' => [
<<< MARKDOWN
# heading 1
MARKDOWN,
<<< HTML
<h1>heading 1</h1>
HTML,
            ],
        ];
    }

    private function assertParseMarkdownAsExpected($markdown, $expected)
    {
        $this->assertEquals($expected, (new MarkdownRenderer())->render($markdown));
    }

    /**
     * @test
     * @dataProvider provideHtmlMarkdown
     */
    public function it_parses_an_html_markdown_block_as_expected(string $markdown, string $expected): void
    {
        $this->assertParseMarkdownAsExpected($markdown, $expected);
    }

    public function provideHtmlMarkdown()
    {
        return [
            'markdown-with-html' => [
<<< MARKDOWN
<div class="block" markdown="1">
- list item1
- list item2
</div>
MARKDOWN,
<<<HTML
<div class="block">
<ul>
<li>list item1</li>
<li>list item2</li>
</ul>
</div>
HTML,
            ],
            'markdown-with-headings-html-list-and-class' => [
<<< MARKDOWN
# heading 1
## heading 2 {.with-class}

<div class="block" markdown="1">
- list item1
- list item2
</div>
MARKDOWN,
<<< HTML
<h1>heading 1</h1>
<h2 class="with-class">heading 2</h2>
<div class="block">
<ul>
<li>list item1</li>
<li>list item2</li>
</ul>
</div>
HTML
            ],
        ];
    }
}
