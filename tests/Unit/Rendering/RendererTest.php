<?php

namespace Tests\Unit\Rendering;

use Macrominds\Config\Config;
use Macrominds\Config\InvalidConfigurationException;
use Macrominds\ContentAccess\ContentPathDoesNotExistException;
use Macrominds\ContentAccess\ContentPathProvider;
use Macrominds\Rendering\Renderer;
use Macrominds\Rendering\TemplateEngine\Context;
use Macrominds\Rendering\TemplateEngine\TemplateNotFoundException;
use Macrominds\Resource\ContentResource;
use Tests\TestCase;
use Tests\Unit\Rendering\RenderingMock\StaticStringTemplateEngine;
use Tests\Unit\Rendering\RenderingMock\TemplateNotFoundTemplateEngine;

class RendererTest extends TestCase
{
    /**
     * @test
     *
     * @throws ContentPathDoesNotExistException
     * @throws InvalidConfigurationException
     * @throws TemplateNotFoundException
     */
    public function it_throws_a_template_not_found_exception_if_the_template_cannot_be_found()
    {
        $configuredContentPath = $this->getContentPath('/general');
        $pathProvider = new ContentPathProvider(new Config([
            ContentPathProvider::CONFIG_KEY_PATH_CONTENT => $configuredContentPath,
        ]));

        $renderer = new Renderer($pathProvider, new TemplateNotFoundTemplateEngine());

        $this->expectException(TemplateNotFoundException::class);

        $renderer->renderPage([
                'content' => 'content',
                'template' => 'standard',
            ],
            new ContentResource('existing', 'no-template', new Context([
                'fallback_type' => 'html',
            ]))
        );
    }

    /**
     * @test
     *
     * @throws ContentPathDoesNotExistException
     * @throws InvalidConfigurationException
     * @throws TemplateNotFoundException
     */
    public function it_throws_a_content_path_does_not_exist_exception_if_the_fallback_template_type_exists()
    {
        $configuredContentPath = $this->getContentPath('/general');
        $pathProvider = new ContentPathProvider(new Config([
            ContentPathProvider::CONFIG_KEY_PATH_CONTENT => $configuredContentPath,
        ]));

        $staticStringTemplateEngine = new StaticStringTemplateEngine();

        $unknownTemplateType = 'no-template';

        $staticStringTemplateEngine->setUnknownContentTypes([$unknownTemplateType]);

        $renderer = new Renderer($pathProvider, $staticStringTemplateEngine);

        $this->expectException(ContentPathDoesNotExistException::class);

        $renderer->renderPage([
            'content' => 'content',
            'template' => 'standard',
        ],
            new ContentResource('existing', $unknownTemplateType, new Context([
                'fallback_type' => 'html',
            ]))
        );
    }
}
