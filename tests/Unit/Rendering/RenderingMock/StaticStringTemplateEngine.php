<?php

namespace Tests\Unit\Rendering\RenderingMock;

use Macrominds\Rendering\TemplateEngine\Context;
use Macrominds\Rendering\TemplateEngine\TemplateEngine;
use Macrominds\Rendering\TemplateEngine\TemplateNotFoundException;

class StaticStringTemplateEngine implements TemplateEngine
{
    /**
     * @var array
     */
    private $unknownContentTypes = [];

    public function setUnknownContentTypes(array $templateTypes): self
    {
        $this->unknownContentTypes = $templateTypes;

        return $this;
    }

    public function render(Context $context, string $contentType): string
    {
        if (in_array($contentType, $this->unknownContentTypes)) {
            throw new TemplateNotFoundException($context->getTemplate().'.'.$contentType);
        }

        return 'static string';
    }
}
