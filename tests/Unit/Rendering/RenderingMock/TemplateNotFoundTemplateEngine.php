<?php

namespace Tests\Unit\Rendering\RenderingMock;

use Macrominds\Rendering\TemplateEngine\Context;
use Macrominds\Rendering\TemplateEngine\TemplateEngine;
use Macrominds\Rendering\TemplateEngine\TemplateNotFoundException;

class TemplateNotFoundTemplateEngine implements TemplateEngine
{
    /**
     * {@inheritdoc}
     */
    public function render(Context $context, string $contentType): string
    {
        throw new TemplateNotFoundException(null);
    }
}
