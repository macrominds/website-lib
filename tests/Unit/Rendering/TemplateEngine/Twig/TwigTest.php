<?php

namespace Tests\Unit\Rendering\TemplateEngine\Twig;

use Macrominds\Helpers\Str\Str;
use Macrominds\Rendering\TemplateEngine\Context;
use Macrominds\Rendering\TemplateEngine\TemplateNotFoundException;
use Macrominds\Rendering\TemplateEngine\Twig\Builder;
use Tests\Setup\SetupTwigEngine;
use Tests\TestCase;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

/**
 * @group template-engine
 * @group twig
 */
class TwigTest extends TestCase
{
    use SetupTwigEngine;

    /**
     * @test
     *
     * @throws LoaderError
     * @throws SyntaxError
     * @throws RuntimeError
     */
    public function it_renders_standard_content_with_template_file()
    {
        $context = new Context([
            'content' => 'hello',
            'title' => 'My Title',
        ]);

        $this->assertEquals(
            $this->getExpectedStandardResult($context),
            $this->setupTwigEngine()->renderFile('standard.html.twig', $context)
        );
    }

    private function getExpectedStandardResult(Context $context)
    {
        return
<<<RESULT
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>{$context['title']}</title>
    </head>
<body>
    {$context['content']}
</body>
</html>
RESULT;
    }

    /**
     * @test
     */
    public function it_locates_the_correct_template_file()
    {
        $twig = $this->setupTwigEngine('/simple');
        $this->assertEquals('standard.html.twig', $twig->locateTemplateFile('standard', 'html'));
        $this->assertEquals('standard.html.twig', $twig->locateTemplateFile('standard', 'html'));
    }

    /**
     * @test
     *
     * @throws LoaderError
     * @throws SyntaxError
     * @throws RuntimeError
     * @throws TemplateNotFoundException
     */
    public function it_renders_standard_content()
    {
        $context = new Context([
            'content' => 'hello',
            'template' => 'standard',
            'title' => 'My Title',
        ]);

        $this->assertEquals(
            $this->getExpectedStandardResult($context),
            $this->setupTwigEngine()->render($context, 'html')
        );
    }

    /**
     * @test
     *
     * @throws LoaderError
     * @throws SyntaxError
     * @throws RuntimeError
     */
    public function it_respects_functions()
    {
        $twig = $this->setupTwigEngine('/general', function (Builder $builder) {
            $builder->addFunction('studlyCase', function ($param) {
                return Str::studly($param);
            });
        });

        $result = $twig->renderTemplate(
            'This is {{ studlyCase("my-studly-case") }}!',
            new Context([])
        );

        $this->assertEquals('This is MyStudlyCase!', $result);
    }

    /**
     * @test
     *
     * @throws LoaderError
     * @throws SyntaxError
     * @throws RuntimeError
     */
    public function it_renders_loops()
    {
        $twig = $this->setupTwigEngine('/simple');
        $items = ['item 1', 'item 2', 'item 3'];
        $result = $twig->renderFile('loop.txt.twig', new Context([
            'loop' => $items,
        ]));
        $this->assertEquals(join('', $items), $result);
    }

    /**
     * @test
     *
     * @throws LoaderError
     * @throws SyntaxError
     * @throws RuntimeError
     * @throws TemplateNotFoundException
     */
    public function it_renders_as_template()
    {
        $twig = $this->setupTwigEngine('/general');
        $result = $twig->render(new Context([
            'title' => 'is_template',
            'is_template' => true,
            'template' => 'standard',
            'content' => '{{ title }}',
        ]), 'html');
        $expected = <<< HTML
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>is_template</title>
    </head>
<body>
    is_template
</body>
</html>
HTML;
        $this->assertEquals($expected, $result);
    }
}
