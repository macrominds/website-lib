<?php

namespace Tests\Unit\Rendering\TemplateEngine\Twig;

use Macrominds\Config\Config;
use Macrominds\Config\InvalidConfigurationException;
use Macrominds\Rendering\TemplateEngine\TemplateEngine;
use Macrominds\Rendering\TemplateEngine\TemplateEngineFactory;
use Macrominds\Rendering\TemplateEngine\Twig\TwigTemplateEngineFactory;
use Tests\TestCase;

class TwigTemplateEngineFactoryTest extends TestCase
{
    /**
     * @test
     *
     * @throws InvalidConfigurationException
     */
    public function it_creates_a_template_engine_for_a_valid_configuration()
    {
        $templateEngine = (new TwigTemplateEngineFactory(new Config([
            TemplateEngineFactory::CONFIG_KEY_PATH_TEMPLATE => $this->getTemplatePath('/general'),
            TemplateEngineFactory::CONFIG_KEY_PATH_CACHE => $this->getCachePath(),
        ])))->create();
        $this->assertInstanceOf(TemplateEngine::class, $templateEngine);
    }

    /**
     * @test
     *
     * @throws InvalidConfigurationException
     * @dataProvider provideInvalidConfiguration
     */
    public function it_throws_appropriate_exceptions_for_invalid_configurations(callable $configFactory, string $expectedMessage)
    {
        $factory = new TwigTemplateEngineFactory(new Config($configFactory()));
        $this->expectException(InvalidConfigurationException::class);
        $this->expectExceptionMessageMatches($expectedMessage);
        $factory->create();
    }

    public function provideInvalidConfiguration(): array
    {
        return [
            'missing cache path' => [
                function (): array {
                    return [
                        TemplateEngineFactory::CONFIG_KEY_PATH_TEMPLATE => $this->getTemplatePath('/general'),
                    ];
                },
                '/Configuration \''.TemplateEngineFactory::CONFIG_KEY_PATH_CACHE.'\' missing or invalid/',
            ],
            'missing template path' => [
                function (): array {
                    return [
                        TemplateEngineFactory::CONFIG_KEY_PATH_CACHE => $this->getCachePath(),
                    ];
                },
                '/Configuration \''.TwigTemplateEngineFactory::CONFIG_KEY_PATH_TEMPLATES.'\' missing or invalid/',
            ],
            'both, cache and template path missing' => [
                function (): array {
                    return [
                    ];
                },
                '/Configuration \'('.
                TemplateEngineFactory::CONFIG_KEY_PATH_CACHE.
                '|'.
                TwigTemplateEngineFactory::CONFIG_KEY_PATH_TEMPLATES.
                ')\' missing or invalid/',
            ],
            'null cache path' => [
                function (): array {
                    return [
                        TemplateEngineFactory::CONFIG_KEY_PATH_TEMPLATE => $this->getTemplatePath('/general'),
                        TemplateEngineFactory::CONFIG_KEY_PATH_CACHE => null,
                    ];
                },
                '/Configuration \''.TemplateEngineFactory::CONFIG_KEY_PATH_CACHE.'\' missing or invalid/',
            ],
            'null template path' => [
                function (): array {
                    return [
                        TemplateEngineFactory::CONFIG_KEY_PATH_TEMPLATE => null,
                        TemplateEngineFactory::CONFIG_KEY_PATH_CACHE => $this->getCachePath(),
                    ];
                },
                '/Configuration \''.TwigTemplateEngineFactory::CONFIG_KEY_PATH_TEMPLATES.'\' missing or invalid/',
            ],
            'both, cache and template path null' => [
                function (): array {
                    return [
                        TemplateEngineFactory::CONFIG_KEY_PATH_TEMPLATE => null,
                        TemplateEngineFactory::CONFIG_KEY_PATH_CACHE => null,
                    ];
                },
                '/Configuration \'('.
                TemplateEngineFactory::CONFIG_KEY_PATH_CACHE.
                '|'.
                TwigTemplateEngineFactory::CONFIG_KEY_PATH_TEMPLATES.
                ')\' missing or invalid/',
            ],
        ];
    }
}
