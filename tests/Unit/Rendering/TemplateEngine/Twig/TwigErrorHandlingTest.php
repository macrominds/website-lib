<?php

namespace Tests\Unit\Rendering\TemplateEngine\Twig;

use Macrominds\Rendering\TemplateEngine\Context;
use Macrominds\Rendering\TemplateEngine\TemplateNotFoundException;
use Macrominds\Rendering\TemplateEngine\Twig\Twig;
use Tests\RestoresErrorLevel;
use Tests\Setup\SetupTwigEngine;
use Tests\TestCase;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

/**
 * @group template-engine
 * @group twig
 * @group error-handling
 */
class TwigErrorHandlingTest extends TestCase
{
    use SetupTwigEngine;
    use RestoresErrorLevel;

    /**
     * @var Twig
     */
    private $twig;

    public function setUp(): void
    {
        $this->twig = $this->setupTwigEngine();
        parent::setUp();
    }

    /**
     * @test
     *
     * @throws LoaderError
     * @throws SyntaxError
     * @throws RuntimeError
     */
    public function it_throws_an_exception_when_an_unexported_function_is_called()
    {
        $this->expectException(SyntaxError::class);
        $this->twig->renderTemplate('{{ unexportedFunction() }}', new Context([]));
    }

    /**
     * @test
     *
     * @throws TemplateNotFoundException
     * @throws SyntaxError
     * @throws RuntimeError
     */
    public function it_throws_an_exception_when_a_template_cannot_be_found()
    {
        $this->expectException(TemplateNotFoundException::class);
        $this->expectExceptionMessage('Unable to find template "non-existing-template.html.twig"');
        $this->twig->render(
            (new Context())
                ->setTemplate('non-existing-template'),
            'html'
        );
    }

    /**
     * @test
     *
     * @throws LoaderError
     * @throws SyntaxError
     * @throws RuntimeError
     */
    public function it_throws_an_exception_when_the_template_syntax_is_wrong()
    {
        $this->expectException(SyntaxError::class);
        $this->twig->renderTemplate('{{ unexportedFunction() %}', new Context([]));
    }

    /**
     * @test
     *
     * @throws LoaderError
     * @throws SyntaxError
     * @throws RuntimeError
     */
    public function it_throws_a_runtime_error_when_we_have_an_array_to_string_conversion_and_error_reporting_e_all()
    {
        error_reporting(E_ALL);
        $this->expectException(RuntimeError::class);
        $this->twig->renderTemplate('{{ test }}', new Context([
            'test' => ['test'],
        ]));
    }

    /**
     * @test
     *
     * @throws LoaderError
     * @throws SyntaxError
     * @throws RuntimeError
     */
    public function it_throws_a_runtime_error_when_we_have_an_object_to_string_conversion()
    {
        $this->expectException(RuntimeError::class);
        $this->twig->renderTemplate('{{ test }}', new Context([
            'test' => new class() {
            },
        ]));
    }
}
