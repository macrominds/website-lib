<?php

namespace Tests\Unit\Rendering\TemplateEngine\Twig;

use Macrominds\Rendering\TemplateEngine\Context;
use Macrominds\Rendering\TemplateEngine\TemplateNotFoundException;
use Macrominds\Rendering\TemplateEngine\Twig\Builder;
use Tests\TestCase;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

/**
 * @group template-engine
 * @group twig
 */
class TwigBuilderTest extends TestCase
{
    const SUB_FOLDER_SIMPLE = '/simple';

    /**
     * @test
     */
    public function it_stores_the_cache_path()
    {
        $expectedCachePath = $this->getCachePath();
        $builder = new Builder($expectedCachePath);
        $this->assertEquals($expectedCachePath, $builder->getCachePath());
    }

    /**
     * @test
     */
    public function it_stores_the_template_paths()
    {
        $builder = new Builder($this->getCachePath());

        $expectedTemplatePaths = [
            $this->getTemplatePath(self::SUB_FOLDER_SIMPLE),
            $this->getTemplatePath('/general'),
        ];

        $builder->setTemplatePaths($expectedTemplatePaths);

        $this->assertEquals($expectedTemplatePaths, $builder->getTemplatePaths());
    }

    /**
     * @test
     */
    public function it_stores_the_debug_mode()
    {
        $builder = new Builder($this->getCachePath());
        $this->assertFalse($builder->getDebugMode());
        $builder->setDebugMode(true);
        $this->assertTrue($builder->getDebugMode());
        $builder->setDebugMode(false);
        $this->assertFalse($builder->getDebugMode());
    }

    /**
     * @test
     */
    public function it_stores_the_functions()
    {
        $builder = new Builder($this->getCachePath());

        $expectedFunction = function ($param) {
            return $param;
        };

        $builder->addFunction('theFunctionName', $expectedFunction);

        $this->assertCount(1, $builder->getFunctions());
        $actualFunction = $builder->getFunctions()['theFunctionName'];

        $this->assertEquals($expectedFunction, $actualFunction);
        $this->assertEquals('param', $actualFunction('param'));
    }

    /**
     * @test
     * @dataProvider provideContentAndTemplate
     *
     * @throws LoaderError
     * @throws SyntaxError
     * @throws RuntimeError
     * @throws TemplateNotFoundException
     */
    public function it_builds_a_twig_template_engine($content, $template, $expected)
    {
        $builder = new Builder($this->getCachePath());
        $builder->setTemplatePaths([$this->getTemplatePath(self::SUB_FOLDER_SIMPLE)]);
        $templateEngine = $builder->build();

        $result = $templateEngine->render(
            new Context([
                'content' => $content,
                'template' => $template,
            ]),
            'txt'
        );

        $this->assertEquals($expected, $result);
    }

    public function provideContentAndTemplate()
    {
        return [
            'a-in-a-out' => [
                'Hello, Twig!',
                'standard',
                'Hello, Twig!',
            ],
            'a-in-b-out' => [
                'Hello, Twig!',
                'intro-content-outro',
                'intro Hello, Twig! outro',
            ],
        ];
    }
}
