<?php

namespace Tests\Unit\Rendering\TemplateEngine;

use Macrominds\Helpers\Str\Str;
use Macrominds\Rendering\TemplateEngine\Context;
use Tests\TestCase;

/**
 * @group template-engine-context
 */
class ContextTest extends TestCase
{
    /**
     * @test
     * @dataProvider provideValidArrayAccessKeyValuePair
     */
    public function it_has_array_access($key, $value)
    {
        $context = new Context();
        $expectedContent = $value;
        $context[$key] = $expectedContent;
        $this->assertEquals($expectedContent, $context[$key]);

        $this->assertTrue(isset($context[$key]));
        unset($context[$key]);
        $this->assertFalse(isset($context[$key]));
    }

    public function provideValidArrayAccessKeyValuePair()
    {
        return [
          'key "content"' => [
              'content',
              'my-content',
          ],
          'unknown key' => [
              'my-extension',
              'my extension content',
          ],
        ];
    }

    /**
     * @test
     */
    public function it_provides_methods_for_common_fields()
    {
        $context = new Context();

        $this->setAndAssertField($context, 'content', 'Hello, World!');
        $this->setAndAssertField($context, 'template', 'standard');
        $this->setAndAssertField($context, 'is_template', true);
    }

    public function setAndAssertField(Context $context, string $field, $value)
    {
        $expectedContent = $value;

        $ucField = Str::studly($field);
        $setterMethodName = "set{$ucField}";
        $context->$setterMethodName($expectedContent);
        $this->assertEquals($expectedContent, $context[$field]);

        $getterMethodName = "get{$ucField}";
        $this->assertEquals($expectedContent, $context->$getterMethodName());
    }

    /**
     * @test
     */
    public function it_returns_an_empty_string_for_unset_content()
    {
        $context = new Context();
        $this->assertEquals('', $context->getContent());
    }

    /**
     * @test
     */
    public function it_can_be_created_from_array()
    {
        $expectedContent = 'my-content';
        $expectedIsTemplate = false;
        $expectedTemplate = 'standard';

        $context = new Context([
            'content' => $expectedContent,
            'is_template' => false,
            'template' => 'standard',
        ]);

        $this->assertEquals($expectedContent, $context->getContent());
        $this->assertEquals($expectedIsTemplate, $context->getIsTemplate());
        $this->assertEquals($expectedTemplate, $context->getTemplate());
    }

    /**
     * @test
     * @dataProvider provideValidArrayAccessKeyValuePair
     */
    public function it_can_be_converted_to_array($key, $value)
    {
        $expected = [$key => $value];
        $actual = (new Context($expected))->toArray();
        $this->assertEquals($expected, $actual);
    }
}
