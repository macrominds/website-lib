<?php

namespace Tests\Unit\Helpers;

use Macrominds\Helpers\PathInfo;
use Tests\TestCase;

/**
 * @group path-info
 */
class PathInfoTest extends TestCase
{
    /**
     * @test
     * @dataProvider providePrefixAndSuffix
     */
    public function it_extracts_the_filename_from_a_path($prefix, $suffix)
    {
        $filename = "{$prefix}.{$suffix}";

        $this->assertEquals($prefix, (new PathInfo($filename))->getFilenameWithoutSuffix());
    }

    /**
     * @test
     * @dataProvider providePrefixAndSuffix
     */
    public function it_extracts_the_suffix_from_a_path($prefix, $suffix)
    {
        $filename = "{$prefix}.{$suffix}";

        $this->assertEquals($suffix, (new PathInfo($filename))->getSuffix());
    }

    public function providePrefixAndSuffix()
    {
        return [
            'index.html' => [
                'index',
                'html',
            ],
            'existing.yml.md' => [
                'existing',
                'yml.md',
            ],
        ];
    }

    /**
     * @test
     */
    public function it_returns_the_path_as_the_prefix_and_null_as_the_suffix_if_we_have_no_suffix()
    {
        $filename = 'no-suffix';
        $pathInfo = (new PathInfo($filename));
        $this->assertEquals($filename, $pathInfo->getFilenameWithoutSuffix());
        $this->assertNull($pathInfo->getSuffix());
    }
}
