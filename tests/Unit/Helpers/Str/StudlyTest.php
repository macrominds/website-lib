<?php

namespace Tests\Unit\Helpers\Str;

use Macrominds\Helpers\Str\Studly;
use Tests\TestCase;

/**
 * @group str
 */
class StudlyTest extends TestCase
{
    /**
     * @test
     * @dataProvider provideSimpleExamples
     * @dataProvider provideEmptyStringResults
     */
    public function it_converts_a_string_to_studly_case($source, $studlyCase)
    {
        $this->assertEquals($studlyCase, (new Studly($source))->transform());
    }

    public function provideSimpleExamples()
    {
        return [
            'i' => [
                'i',
                'I',
            ],
            'i can' => [
                'i can',
                'ICan',
            ],
            'i_can_convert' => [
                'i_can_convert',
                'ICanConvert',
            ],
        ];
    }

    public function provideEmptyStringResults()
    {
        return [
            '""' => [
                '',
                '',
            ],
            '-' => [
                '-',
                '',
            ],
            '_' => [
                '_',
                '',
            ],
            ' ' => [
                ' ',
                '',
            ],
        ];
    }
}
