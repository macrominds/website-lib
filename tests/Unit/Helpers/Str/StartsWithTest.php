<?php

namespace Tests\Unit\Helpers\Str;

use InvalidArgumentException;
use Macrominds\Helpers\Str\StartsWith;
use Tests\TestCase;

/**
 * @group str
 */
class StartsWithTest extends TestCase
{
    /**
     * @test
     * @dataProvider provideStringThatStartsWithAnotherString
     */
    public function it_detects_if_a_string_starts_with_another(string $haystack, string $needle)
    {
        $this->assertTrue((new StartsWith($haystack))->check($needle));
    }

    public function provideStringThatStartsWithAnotherString()
    {
        // TODO rename to improve revealing the intent
        return $this->addKeys([
            [
                '/path-with-leading-slash',
                '/',
            ],
            [
                '/path/with/leading/part',
                '/path/with/leading/',
            ],
            [
                'equal',
                'equal',
            ],
        ]);
    }

    private function addKeys(array $array): array
    {
        return array_combine(array_map(function ($tupel) {
            return "🏁 '{$tupel[1]}' in '{$tupel[0]}'";
        }, $array), $array);
    }

    /**
     * @test
     * @dataProvider provideStringThatDoesntStartWithAnotherString
     */
    public function it_detects_if_a_string_doesnt_start_with_another(string $haystack, string $needle)
    {
        $this->assertFalse((new StartsWith($haystack))->check($needle));
    }

    public function provideStringThatDoesntStartWithAnotherString()
    {
        return $this->addKeys([
            [
                'path-without-leading-slash',
                '/',
                false,
            ],
            [
                '',
                '/',
                false,
            ],
            [
                'Case-not-matching',
                'case-not',
            ],
        ]);
    }

    /**
     * @test
     * @dataProvider provideInvalidNeedles
     */
    public function it_handles_invalid_needles(string $haystack, string $needle, $expectedException)
    {
        $this->expectException($expectedException);
        (new StartsWith($haystack))->check($needle);
    }

    public function provideInvalidNeedles()
    {
        return $this->addKeys([
            [
                '',
                '',
                InvalidArgumentException::class,
            ],
            [
                '/',
                '',
                InvalidArgumentException::class,
            ],
        ]);
    }
}
