<?php

namespace Tests\Unit\Helpers;

use Macrominds\Helpers\Arr;
use Tests\TestCase;

/**
 * @group arr
 */
class ArrTest extends TestCase
{
    const ARR = [
        'a' => 1,
        'b' => 2,
        'set' => 'value',
        'empty-string' => '',
        'empty-array' => [],
        'null' => null,
        'path' => [
            'template' => 'my-templates',
            'content' => 'my-content',
        ],
        'some' => [
            'levels' => [
                'deep' => [
                    'lies' => [
                        'a' => [
                            'secret' => 'chest',
                        ],
                    ],
                ],
            ],
        ],
        'key.with' => 'dot',
        'key.with.dot' => 'possible',
        'but.dont.mix' => [
            'dots.and.nesting' => 'unless you really know what you do',
        ],
    ];

    /**
     * @test
     * @dataProvider provideFlatAccess
     */
    public function it_can_access_array_values(string $key, $expected)
    {
        $this->assertEquals($expected, Arr::get(self::ARR, $key));
    }

    public function provideFlatAccess(): array
    {
        return [
            'a' => [
                'a',
                1,
            ],
            'b' => [
                'b',
                2,
            ],
            'null' => [
                'null',
                null,
            ],
        ];
    }

    /**
     * @test
     * @dataProvider provideFlatFallback
     */
    public function it_returns_existing_values_or_fallback_for_missing_values(string $key, $fallback, $expected)
    {
        $this->assertEquals($expected, Arr::get(self::ARR, $key, $fallback));
    }

    public function provideFlatFallback(): array
    {
        return [
            'existing value' => [
                'set',
                'fallback',
                'value',
            ],
            'existing empty string' => [
                'empty-string',
                'fallback',
                '',
            ],
            'existing empty array' => [
                'empty-array',
                'fallback',
                [],
            ],
            'existing null value (expects fallback)' => [
                'null',
                'fallback',
                'fallback',
            ],
            'missing value' => [
                'unset',
                'fallback',
                'fallback',
            ],
        ];
    }

    /**
     * @test
     * @dataProvider provideSelectorsForExistingNestedValues
     */
    public function it_can_access_nested_array_values_using_dot_notation($key, $expected)
    {
        $this->assertEquals($expected, Arr::get(self::ARR, $key));
    }

    public function provideSelectorsForExistingNestedValues(): array
    {
        return [
            'path.template' => [
                'path.template',
                'my-templates',
            ],
            'path.content' => [
                'path.content',
                'my-content',
            ],
            'some.levels.deep.lies.a.secret' => [
                'some.levels.deep.lies.a.secret',
                'chest',
            ],
        ];
    }

    /**
     * @test
     * @dataProvider provideSelectorsForFlatKeysWithDots
     */
    public function it_can_access_flat_keys_with_dots($key, $expected)
    {
        $this->assertEquals($expected, Arr::get(self::ARR, $key));
    }

    public function provideSelectorsForFlatKeysWithDots(): array
    {
        return [
            'key.with' => [
                'key.with',
                'dot',
            ],
            'key.with.dot' => [
                'key.with.dot',
                'possible',
            ],
            'but.dont.mix' => [
                'but.dont.mix',
                ['dots.and.nesting' => 'unless you really know what you do'],
            ],
            'but.dont.mix.dots.and.nesting' => [
                'but.dont.mix.dots.and.nesting',
                null,
            ],
        ];
    }

    /**
     * @test
     * @dataProvider provideDotNotationWithFallbacks
     */
    public function it_provides_fallbacks_for_dot_notation($key, $fallback, $expected)
    {
        $this->assertEquals($expected, Arr::get(self::ARR, $key, $fallback));
    }

    public function provideDotNotationWithFallbacks(): array
    {
        return [
            'not.existing' => [
                'not.existing',
                'fallback',
                'fallback',
            ],
            'partially existing: key' => [
                'key',
                'fallback',
                'fallback',
            ],
            'partially existing: key.with.ring' => [
                'key.with.ring',
                'fallback',
                'fallback',
            ],
        ];
    }

    /**
     * @test
     * @dataProvider mergeDataProvider
     */
    public function it_merges_array_nodes_and_replaces_leaves($original, $new, $expected)
    {
        $this->assertEquals($expected, Arr::replaceRecursive($original, $new));
    }

    public function mergeDataProvider(): array
    {
        return [
            'flat structures' => [
                [
                    'old-path' => '/index.html',
                ],
                [
                    'new-number' => 1,
                    'new-string' => 'string',
                ],
                [
                    'old-path' => '/index.html',
                    'new-number' => 1,
                    'new-string' => 'string',
                ],
            ],
            'deep merge' => [
                [
                    'old-category' => [
                        'old-path' => '/index.html',
                    ],
                ],
                [
                    'old-category' => [
                        'new-entry' => 'entry',
                    ],
                ],
                [
                    'old-category' => [
                        'old-path' => '/index.html',
                        'new-entry' => 'entry',
                    ],
                ],
            ],
            'deep merge with replace' => [
                [
                    'old-category' => [
                        'old-path' => '/index.html',
                        'old-value-to-be-replaced' => 'old',
                    ],
                ],
                [
                    'old-category' => [
                        'new-entry' => 'entry',
                        'old-value-to-be-replaced' => 'new',
                    ],
                ],
                [
                    'old-category' => [
                        'old-path' => '/index.html',
                        'old-value-to-be-replaced' => 'new',
                        'new-entry' => 'entry',
                    ],
                ],
            ],
        ];
    }
}
