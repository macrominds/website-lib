<?php

namespace Tests\Unit\Lifecycle;

use Exception;
use Macrominds\ContentAccess\ContentPathDoesNotExistException;
use Macrominds\Lifecycle\AbstractLifecycle;
use Macrominds\Lifecycle\DefaultLifecycle;
use Macrominds\Page\Page;
use Macrominds\Request\ParsedRequest;
use Macrominds\Resource\ContentResource;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Tests\RestoresErrorLevel;
use Tests\TestCase;
use Throwable;

/**
 * @group lifecycle
 */
class AbstractLifecycleTest extends TestCase
{
    use RestoresErrorLevel;

    /**
     * @test
     *
     * @throws Throwable
     */
    public function it_performs_all_steps_in_required_order()
    {
        $monitoringLifecycle = $this->createMonitoringLifecycle(
            function () { return $this->validContentResource(); }
        );
        $monitoringLifecycle->withErrorHandling();
        $monitoringLifecycle->run();
        $this->assertEquals([
            'boot',
            'fetchRequest',
            'parseRequest',
            'rewriteRequest',
            'locateResource',
            'createPage',
            'createResponse',
            'sendResponse',
            'shutdown',
        ], $monitoringLifecycle->methodCallsInOrder());
    }

    private function validContentResource(): ContentResource
    {
        return new ContentResource('/index.yml.md', 'html');
    }

    private function createMonitoringLifecycle(callable $locateResource)
    {
        return new class($locateResource) extends AbstractLifecycle {
            private $methodCalls = [];
            private $locateResourceCallback;

            public function __construct(callable $locateResource)
            {
                $this->locateResourceCallback = $locateResource;
            }

            public function methodCallsInOrder(): array
            {
                return $this->methodCalls;
            }

            protected function boot()
            {
                $this->monitor(__FUNCTION__);
            }

            private function monitor($method, /* @noinspection PhpUnusedParameterInspection */...$parameters)
            {
                $this->methodCalls[] = $method;
            }

            protected function fetchRequest(): Request
            {
                $this->monitor(__FUNCTION__);

                return Request::create('/');
            }

            protected function parseRequest(Request $request): ParsedRequest
            {
                $this->monitor(__FUNCTION__, [$request]);

                return new ParsedRequest(Request::create('/'));
            }

            protected function rewriteRequest(ParsedRequest $parsedRequest): ParsedRequest
            {
                $this->monitor(__FUNCTION__, [$parsedRequest]);

                return $parsedRequest;
            }

            protected function locateResource(ParsedRequest $rewrittenRequest): ContentResource
            {
                $this->monitor(__FUNCTION__, [$rewrittenRequest]);
                $callback = $this->locateResourceCallback;

                return $callback($rewrittenRequest);
            }

            protected function createPage(ContentResource $resource): Page
            {
                $this->monitor(__FUNCTION__, [$resource]);

                return new Page(
                    '<h1>Hello World</h1>',
                    [
                        'status_code' => 200,
                    ]
                );
            }

            protected function createResponse(Page $page): Response
            {
                $this->monitor(__FUNCTION__, [$page]);

                return new Response($page->getContent());
            }

            protected function sendResponse(Response $response): Response
            {
                $this->monitor(__FUNCTION__, [$response]);

                return $response/*->send()*/;
            }

            protected function shutdown(Response $response, callable $onShutdown = null)
            {
                $this->monitor(__FUNCTION__, [$response]);
            }

            protected function createErrorResponse(Throwable $e, ParsedRequest $parsedRequestOrNull = null): Response
            {
                $this->monitor(__FUNCTION__, [$e, $parsedRequestOrNull]);

                return new Response($e->getMessage(), 500);
            }
        };
    }

    /**
     * @test
     * @dataProvider provideExceptionForLocateResource
     *
     * @throws Throwable
     */
    public function it_handles_exceptions($exceptionToBeThrown)
    {
        $monitoringLifecycle = $this->createMonitoringLifecycle(
            function () use ($exceptionToBeThrown) {
                throw $exceptionToBeThrown;
            }
        );
        $monitoringLifecycle->withErrorHandling();
        $monitoringLifecycle->run();
        $this->assertEquals([
            'boot',
            'fetchRequest',
            'parseRequest',
            'rewriteRequest',
            'locateResource',
            'createErrorResponse',
            'sendResponse',
            'shutdown',
        ], $monitoringLifecycle->methodCallsInOrder());
    }

    public function provideExceptionForLocateResource()
    {
        return [
            'ContentPathDoesNotExistException' => [
                new ContentPathDoesNotExistException(
                    '/a-path.html',
                    'content-dir'
                ),
            ],
            'simple Exception' => [
                new Exception('message'),
            ],
        ];
    }

    /**
     * @test
     * @dataProvider provideCallableThatForcesNotice
     *
     * @throws Throwable
     */
    public function it_handles_notices($callableThatForcesNotice)
    {
        error_reporting(E_ALL);
        $monitoringLifecycle = $this->createMonitoringLifecycle(
            function () use ($callableThatForcesNotice): ContentResource {
                $callableThatForcesNotice();

                return $this->validContentResource();
            }
        );
        $monitoringLifecycle->withErrorHandling();
        $monitoringLifecycle->run();
        $this->assertEquals([
            'boot',
            'fetchRequest',
            'parseRequest',
            'rewriteRequest',
            'locateResource',
            'createErrorResponse',
            'sendResponse',
            'shutdown',
        ], $monitoringLifecycle->methodCallsInOrder());
    }

    public function provideCallableThatForcesNotice()
    {
        return [
            'Forced notice: Undefined variable' => [
                function () {
                    /* @noinspection PhpUndefinedVariableInspection */
                    return $unknown;
                },
            ],
            'Forced notice: Undefined index' => [
                function () {
                    /** @noinspection PhpUndefinedVariableInspection */
                    $array = [];

                    return $array['unknown'];
                },
            ],
        ];
    }

    /**
     * @test
     * @dataProvider provideExceptionForLocateResource
     *
     * @throws Throwable
     */
    public function the_error_handling_can_be_turned_off($exceptionToBeThrown)
    {
        $monitoringLifecycle = $this->createMonitoringLifecycle(
            function () use ($exceptionToBeThrown) {
                throw $exceptionToBeThrown;
            }
        );
        $monitoringLifecycle->withoutErrorHandling();
        $this->expectException(get_class($exceptionToBeThrown));
        $monitoringLifecycle->run();
    }

    /**
     * @test
     */
    public function it_reports_if_error_handling_is_turned_on_or_off()
    {
        $lifecycle = $this->createNewLifecycle();
        $lifecycle->withErrorHandling();
        $this->assertTrue($lifecycle->hasErrorHandling());
        $lifecycle->withoutErrorHandling();
        $this->assertFalse($lifecycle->hasErrorHandling());
    }

    private function createNewLifecycle(): DefaultLifecycle
    {
        return new DefaultLifecycle($this->getContainer());
    }

    /**
     * @test
     */
    public function error_handling_is_turned_on_by_default()
    {
        $lifecycle = $this->createNewLifecycle();
        $this->assertTrue($lifecycle->hasErrorHandling());
    }
}
