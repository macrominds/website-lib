<?php

namespace Tests\Unit\Lifecycle;

use Macrominds\Lifecycle\ResponseFactory;
use Macrominds\Page\Page;
use Tests\TestCase;

class ResponseFactoryTest extends TestCase
{
    /**
     * @test
     */
    public function it_creates_a_proper_response_for_a_page()
    {
        $response = (new ResponseFactory())->createResponseFor(
            new Page('content', [])
        );
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals('content', $response->getContent());
    }

    /**
     * @test
     */
    public function it_creates_a_proper_redirect_response()
    {
        $response = (new ResponseFactory())->createResponseFor(
            new Page('content', ['redirect.location' => '/index.html'])
        );
        $this->assertEquals(302, $response->getStatusCode());
        $this->assertEquals('/index.html', $response->headers->get('Location'));
    }

    /**
     * @test
     */
    public function it_creates_a_proper_301_redirect_response()
    {
        $response = (new ResponseFactory())->createResponseFor(
            new Page('content', [
                'redirect.location' => '/test.html',
                'redirect.code' => 301,
            ])
        );
        $this->assertEquals(301, $response->getStatusCode());
        $this->assertEquals('/test.html', $response->headers->get('Location'));
    }
}
