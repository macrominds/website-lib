<?php

namespace Tests\Feature\Lifecycle;

use Macrominds\Config\CommonConfig;
use Macrominds\ContentAccess\ContentPathProvider;
use Macrominds\Logger\LoggerFactory;
use Symfony\Component\HttpFoundation\Response;
use Tests\Feature\WebTestCase;
use Throwable;

/**
 * @group lifecycle
 */
class DefaultLifecycleTest extends WebTestCase
{
    /**
     * @test
     *
     * @throws Throwable
     */
    public function it_responds_with_200_for_existing_resources()
    {
        $response = $this->request('/index.html');
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertStringContainsString('<h1>This is the index page</h1>', $response->getContent());
    }

    /**
     * @test
     *
     * @throws Throwable
     */
    public function it_rewrites_requests_to_root_to_index_html()
    {
        $response = $this->request('/');
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertStringContainsString('<h1>This is the index page</h1>', $response->getContent());
    }

    /**
     * @test
     *
     * @throws Throwable
     * @dataProvider provide404Urls
     */
    public function it_responds_with_404_for_a_non_existing_resource($url)
    {
        $response = $this->request($url);
        $this->assertEquals(404, $response->getStatusCode());
        $this->assertStringContainsString('<h1>Page not found</h1>', $response->getContent());
    }

    public function provide404Urls()
    {
        return $this->indexedValueArrayToAssocDataProvider(
            [
                '/non-existing.html',
                '/non-existing.with-additional-suffix.html',
            ]
        );
    }

    /**
     * @test
     *
     * @throws Throwable
     */
    public function it_responds_with_404_if_the_template_exists_for_the_default_type_but_not_for_the_requested_type()
    {
        $response = $this->request(
            '/existing.xyz'
        );
        $this->assertEquals(404, $response->getStatusCode());
    }

    /**
     * @test
     *
     * @throws Throwable
     */
    public function it_responds_with_500_for_a_non_existing_resource_when_there_is_no_404_page()
    {
        $response = $this->request(
            '/non-existing.html',
            [
                ContentPathProvider::CONFIG_KEY_PATH_CONTENT => $this->getContentPath('/no-404-but-500'),
            ]
        );
        $this->assertEquals(500, $response->getStatusCode());
        $this->assertContainsAllStrings(
            [
                '<h1>Internal Server error</h1>',
                '<h2>Error #0</h2>',
                '404.yml.md not found',
            ],
            $response->getContent()
        );
    }

    /**
     * @test
     *
     * @throws Throwable
     */
    public function it_responds_with_500_for_a_non_existing_resource_when_there_are_neither_500_nor_404_pages()
    {
        $response = $this->request(
            '/non-existing.html',
            [
                ContentPathProvider::CONFIG_KEY_PATH_CONTENT => $this->getContentPath('/empty'),
            ]
        );
        $this->assertEquals(500, $response->getStatusCode());
        $this->assertStringContainsString('500.yml.md' /*<- the exception message*/, $response->getContent());
    }

    /**
     * @test
     *
     * @throws Throwable
     */
    public function it_responds_with_500_in_case_of_an_error()
    {
        $response = $this->request('/error.html');
        $this->assertEquals(500, $response->getStatusCode());
        $this->assertContainsAllStrings(
            [
                '<h1>Internal Server error</h1>',
                '<h2>Error #0</h2>',
                'Unknown &quot;raiseError&quot; function',
            ],
            $response->getContent()
        );
    }

    /**
     * @test
     *
     * @throws Throwable
     */
    public function it_responds_with_500_in_case_of_a_notice()
    {
        $response = $this->request('/notice.html');
        $this->assertEquals(500, $response->getStatusCode());
        $this->assertContainsAllStrings(
            [
                '<h1>Internal Server error</h1>',
                '<h2>Error #0</h2>',
                'An exception has been thrown during the rendering of a template',
                'Array to string conversion',
            ],
            $response->getContent()
        );
    }

    /**
     * @test
     *
     * @throws Throwable
     */
    public function it_can_handle_exceptions_before_we_have_a_parsed_request()
    {
        $this->assertExceptionHandledOutput([
                '<h1>Internal Server error</h1>',
                '<h2>Error #0</h2>',
        ]);
    }

    private function assertExceptionHandledOutput(array $output)
    {
        $response = $this->request(null);
        $this->assertEquals(500, $response->getStatusCode());
        $this->assertContainsAllStrings($output, $response->getContent());
    }

    /**
     * @test
     * @requires PHP >= 8.0
     */
    public function it_dumps_the_expected_cause_for_php_versions_since_8()
    {
        $this->assertExceptionHandledOutput([
            'Return value must be of type Symfony\Component\HttpFoundation\Request, null returned',
        ]);
    }

    /**
     * @test
     *
     * @throws Throwable
     */
    public function it_handles_logger_failures_during_error_response_creation()
    {
        $response = $this->request('/non-existing.html', [
            ContentPathProvider::CONFIG_KEY_PATH_CONTENT => $this->getContentPath('/empty'),
            LoggerFactory::CONFIG_KEY_PATH_LOGGING        => $this->getNonExistingNotCreatableLoggingPath(),
        ]);

        $this->assertEquals(500, $response->getStatusCode(),
            'Failed asserting a 500 status code. This means that visiting the page did not lead to a
            server error as expected. Please make sure to have a 500 status code as a prerequisite for
            this test.');
        $this->assertStringContainsString('Failed to open stream: No such file or directory', $response->getContent());
    }

    /**
     * @test
     * @dataProvider provideErrorUrls
     *
     * @throws Throwable
     */
    public function it_doesnt_expose_exceptions_in_production($uri, ...$errorMessagesToHide)
    {
        $response = $this->request($uri, [CommonConfig::KEY_APP_ENV => 'production']);
        $this->assertEquals(500, $response->getStatusCode());
        $content = $response->getContent();
        foreach ($errorMessagesToHide as $secret) {
            $this->assertStringNotContainsString($secret, $content);
        }
        $this->assertContainsAllStrings(
            [
                '<h1>Internal Server error</h1>',
                'An error occurred. See logs.',
            ],
            $content
        );
    }

    public function provideErrorUrls(): array
    {
        return [
            'null' => [
                null,
                'Return value of class@anonymous::createRequest() must be an instance '.
                'of Symfony\Component\HttpFoundation\Request, null returned',
            ],
            '/error.html' => [
                '/error.html',
                'Unknown &quot;raiseError&quot; function',
            ],
            '/notice.html' => [
                '/notice.html',
                'An exception has been thrown during the rendering of a template',
                'Array to string conversion',
            ],
        ];
    }

    private function getNonExistingNotCreatableLoggingPath(): string
    {
        $file = '/empty/.gitkeep';
        $impossibleSubfile = "{$file}/impossible-log-location.log";

        return $this->getContentPath($impossibleSubfile);
    }

    /**
     * @test
     * @dataProvider provideRedirectUrlAndExpectedStatusCode
     *
     * @throws Throwable
     */
    public function it_redirects_with_the_appropriate_redirect_status_code($url, $code)
    {
        $response = $this->request($url);
        $this->assertEquals($code, $response->getStatusCode());
    }

    public function provideRedirectUrlAndExpectedStatusCode(): array
    {
        return [
            ' 301 ' => [
                '/redirect-301-to-index.html',
                301,
            ],
            ' 302 ' => [
                '/redirect-302-to-index.html',
                302,
            ],
            'default' => [
                '/redirect-default-to-index.html',
                302,
            ],
        ];
    }

    /**
     * @test
     *
     * @throws Throwable
     */
    public function it_redirects_with_the_appropriate_redirect_location()
    {
        $response = $this->followRedirect($this->request('/redirect-301-to-index.html'));
        $this->assertEquals(<<<HTML
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>index</title>
    </head>
<body>
    <h1>This is the index page</h1>
</body>
</html>
HTML,
        $response->getContent());
    }

    /**
     * @throws Throwable
     */
    public function followRedirect(Response $response): Response
    {
        if ($response->isRedirect()) {
            $this->resetContainerProvider();

            return $this->request($response->headers->get('Location'));
        }

        return $response;
    }
}
