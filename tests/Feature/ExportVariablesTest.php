<?php

namespace Tests\Feature;

use Throwable;

class ExportVariablesTest extends WebTestCase
{
    /**
     * @test
     *
     * @throws Throwable
     */
    public function the_lifecycle_exports_variables()
    {
        $this->withoutErrorHandling();
        $response = $this->request('/using-exported-variables.html', [
           'variables' => [
               'var' => [
                   'alien' => '👽',
               ],
               'template' => 'standard',
           ],
        ]);
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertContainsAllStrings(
            [
                '<h1>This uses an 👽 variable</h1>',
            ],
            $response->getContent()
        );
    }
}
