<?php

namespace Tests\Feature;

use Macrominds\App;
use Macrominds\Config\DotEnvFileNotFoundException;
use Macrominds\ContentAccess\ContentPathProvider;
use Macrominds\Frontmatter\FrontmatterServiceProvider;
use Macrominds\Lifecycle\AbstractLifecycle;
use Macrominds\Logger\LoggerFactory;
use Macrominds\MarkdownServiceProvider;
use Macrominds\Rendering\TemplateEngine\TemplateEngineFactory;
use Macrominds\TwigServiceProvider;
use Macrominds\YamlParserServiceProvider;
use Symfony\Component\HttpFoundation\Response;
use Tests\Mock\DefaultLifecycleThatDoesntSendAndDoesntExitHard;
use Tests\TestCase;

class AppTest extends TestCase
{
    /**
     * @test
     *
     * @throws DotEnvFileNotFoundException
     */
    public function it_provides_the_complete_default_lifecycle()
    {
        // TODO split into single assertions
        $result = [];
        $configurationArray = $this->getConfigurationArray();
        $app = (new App($this->getProjectRoot()));
        $container = $app->getContainer();
        // TODO test without these additional service providers
        $app->registerServiceProviders([
            new FrontmatterServiceProvider($configurationArray),
            new YamlParserServiceProvider($configurationArray),
            new MarkdownServiceProvider($configurationArray),
            new TwigServiceProvider($configurationArray),
        ]);
        $container->singleton(
            AbstractLifecycle::class,
            function () use ($container) {
                return new DefaultLifecycleThatDoesntSendAndDoesntExitHard($container);
            }
        );

        $app->configure($configurationArray)
            ->run(
                function (Response $response) use (&$result) {
                    $result['response'] = $response;
                }
            );
        $this->assertArrayHasKey('response', $result);
        /**
         * @var Response
         */
        $response = $result['response'];
        $this->assertNotNull($response);
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals(<<<HTML
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>index</title>
    </head>
<body>
    <h1>This is the index page</h1>
</body>
</html>
HTML,
     $response->getContent());
    }

    private function getConfigurationArray(): array
    {
        return [
            LoggerFactory::CONFIG_KEY_PATH_LOGGING           => 'php://memory',
            ContentPathProvider::CONFIG_KEY_PATH_CONTENT => $this->getContentPath('/general'),
            TemplateEngineFactory::CONFIG_KEY_PATH_CACHE => $this->getCachePath(),
            'variables'                                      => [
                'template' => 'standard',
            ],
            TemplateEngineFactory::CONFIG_KEY_PATH_TEMPLATE => $this->getTemplatePath('/general'),
        ];
    }
}
