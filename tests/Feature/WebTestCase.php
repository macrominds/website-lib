<?php

namespace Tests\Feature;

use Macrominds\BasicServiceProvider;
use Macrominds\Config\CommonConfig;
use Macrominds\ContentAccess\ContentPathProvider;
use Macrominds\Frontmatter\FrontmatterServiceProvider;
use Macrominds\Lifecycle\DefaultLifecycle;
use Macrominds\Logger\LoggerFactory;
use Macrominds\MarkdownServiceProvider;
use Macrominds\Rendering\TemplateEngine\TemplateEngineFactory;
use Macrominds\Rendering\TemplateEngine\Twig\TwigTemplateEngineFactory;
use Macrominds\Services\Container;
use Macrominds\TwigServiceProvider;
use Macrominds\YamlParserServiceProvider;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Tests\Mock\DefaultLifecycleThatDoesntSendAndDoesntExitHard;
use Tests\TestCase;
use Throwable;

class WebTestCase extends TestCase
{
    /**
     * @var bool
     */
    private $shouldHandleErrors = true;

    /**
     * @throws Throwable
     */
    protected function request(string $requestUri = null, array $configOverride = []): Response
    {
        $result = [];
        $lifecycle = $this->setupDefaultLifecycle($requestUri, $configOverride);
        $this->shouldHandleErrors ? $lifecycle->withErrorHandling() : $lifecycle->withoutErrorHandling();
        $lifecycle->run(function (Response $response) use (&$result) {
            $result['response'] = $response;
        });

        return $result['response'];
    }

    protected function setupDefaultLifecycle(?string $requestUri, array $configOverride = []): DefaultLifecycle
    {
        $config =
            array_merge([
                CommonConfig::KEY_APP_ENV                       => 'testing',
                ContentPathProvider::CONFIG_KEY_PATH_CONTENT => $this->getContentPath('/general'),
                TemplateEngineFactory::CONFIG_KEY_PATH_TEMPLATE => $this->getTemplatePath('/general'),
                TemplateEngineFactory::CONFIG_KEY_PATH_CACHE => $this->getCachePath(),
                LoggerFactory::CONFIG_KEY_PATH_LOGGING          => 'php://memory',
                TwigTemplateEngineFactory::CONFIG_KEY_DEBUG_MODE    => true,
                CommonConfig::KEY_SUFFIX_CONTENT_FILE           => 'yml.md',
                CommonConfig::KEY_DEFAULT_RESPONSE_TYPE         => 'html',
                'variables'                                     => ['template' => 'standard'],
            ], $configOverride);

        return new DefaultLifecycleThatDoesntSendAndDoesntExitHard(
            $this->setupContainer($config, $requestUri)
        );
    }

    protected function setupContainer(array $config, string $requestUri = null): Container
    {
        $container = $this->getContainer();
        // TODO test without these sophisticated service providers.
        // Maybe very simple implementations do suffice.
        $container->registerServiceProvider(new BasicServiceProvider($config));
        $container->registerServiceProvider(new YamlParserServiceProvider($config));
        $container->registerServiceProvider(new TwigServiceProvider($config));
        $container->registerServiceProvider(new FrontmatterServiceProvider($config));
        $container->registerServiceProvider(new MarkdownServiceProvider($config));
        $this->registerRequest($requestUri);

        return $container;
    }

    protected function registerRequest(string $requestUri = null): void
    {
        $container = $this->getContainer();
        $container->factoryObject(
            Request::class,
            function () use ($requestUri) {
                if (null === $requestUri) {
                    return null;
                }

                return Request::create($requestUri);
            }
        );
    }

    protected function assertContainsAllStrings(array $expectedStrings, string $actual): void
    {
        foreach ($expectedStrings as $expected) {
            $this->assertStringContainsString($expected, $actual,
                "Failed asserting that {$actual} contains {$expected}");
        }
    }

    protected function withoutErrorHandling()
    {
        $this->shouldHandleErrors = false;
    }
}
