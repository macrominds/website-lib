<?php

namespace Tests\Feature\Config;

use Macrominds\ContentAccess\ContentPathProvider;
use Macrominds\Rendering\TemplateEngine\TemplateEngineFactory;
use Macrominds\Rendering\TemplateEngine\Twig\TwigTemplateEngineFactory;
use Tests\Feature\WebTestCase;
use Throwable;

/**
 * @group config
 * @group wiring
 */
class MissingAndInvalidConfigReportingTest extends WebTestCase
{
    /**
     * @test
     * @dataProvider provideMissingConfigKeyWithExpectedPartOfExceptionMessage
     *
     * @throws Throwable
     */
    public function it_reports_missing_config_with_missing($missingConfigKey, $expectedPartOfExceptionMessage)
    {
        $response = $this->request('/', [
            $missingConfigKey => null,
        ]);
        $this->assertEquals(500, $response->getStatusCode());
        $this->assertStringContainsString($expectedPartOfExceptionMessage, $response->getContent());
    }

    public function provideMissingConfigKeyWithExpectedPartOfExceptionMessage(): array
    {
        // Notice, we're excluding logging configuration intentionally, because the monitored results would differ
        // from the rest. See InvalidLoggingConfigurationExceptionTest for that.

        return [
            'variables' => [
                'variables',
                'Configuration \'variables\' missing or invalid.',
            ],
            ContentPathProvider::CONFIG_KEY_PATH_CONTENT => [
                ContentPathProvider::CONFIG_KEY_PATH_CONTENT,
                'Configuration \''.ContentPathProvider::CONFIG_KEY_PATH_CONTENT.'\' missing or invalid.',
            ],
            TemplateEngineFactory::CONFIG_KEY_PATH_TEMPLATE => [
                TemplateEngineFactory::CONFIG_KEY_PATH_TEMPLATE,
                'Configuration \''.TwigTemplateEngineFactory::CONFIG_KEY_PATH_TEMPLATES.'\' missing or invalid.',
            ],
            TemplateEngineFactory::CONFIG_KEY_PATH_CACHE => [
                TemplateEngineFactory::CONFIG_KEY_PATH_CACHE,
                'Configuration \''.TemplateEngineFactory::CONFIG_KEY_PATH_CACHE.'\' missing or invalid.',
            ],
        ];
    }
}
