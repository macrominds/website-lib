<?php

namespace Tests\Feature;

use Macrominds\ContentAccess\ContentPathProvider;
use Macrominds\Rendering\TemplateEngine\Twig\TwigTemplateEngineFactory;

class TwigTest extends WebTestCase
{
    /** @test */
    public function it_renders_simple_content_with_simple_template()
    {
        $response = $this->request('/existing.html', [
            ContentPathProvider::CONFIG_KEY_PATH_CONTENT => $this->getContentPath('/other'),
            TwigTemplateEngineFactory::CONFIG_KEY_PATH_TEMPLATES => $this->getTemplatePath('/general'),
        ]);
        $this->assertSame(200, $response->getStatusCode());
        $this->assertSame(<<<HTML
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>other existing</title>
    </head>
<body>
    <h1>Other existing</h1>
</body>
</html>
HTML
, $response->getContent());
    }
}
