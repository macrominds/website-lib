<?php

namespace Tests\Feature;

use Macrominds\ContentAccess\ContentPathProvider;
use Macrominds\Rendering\TemplateEngine\Twig\TwigTemplateEngineFactory;
use Throwable;

class MultipleTemplatePathsTest extends WebTestCase
{
    /**
     * @test
     * @dataProvider provideMultipleTemplatePathsInDifferentOrder
     *
     * @throws Throwable
     */
    public function it_respects_multiple_template_paths(array $templates)
    {
        $response = $this->request('/content-with-alien-template.html', [
           ContentPathProvider::CONFIG_KEY_PATH_CONTENT => $this->getContentPath('/content-with-alien-template'),
           TwigTemplateEngineFactory::CONFIG_KEY_PATH_TEMPLATES => $templates,
        ]);
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertStringContainsString('👽 <p>Hello Aliens</p> 👽', $response->getContent());
    }

    public function provideMultipleTemplatePathsInDifferentOrder()
    {
        return [
            'alien, then general' => [
                [
                    $this->getTemplatePath('/alien'),
                    $this->getTemplatePath('/general'),
                ],
            ],
            'general, then alien' => [
                [
                    $this->getTemplatePath('/general'),
                    $this->getTemplatePath('/alien'),
                ],
            ],
        ];
    }
}
