<?php

namespace Tests\Feature;

use Macrominds\Rendering\TemplateEngine\Twig\TwigTemplateEngineFactory;
use Throwable;

class ExportFunctionsTest extends WebTestCase
{
    /**
     * @test
     *
     * @throws Throwable
     */
    public function the_lifecycle_exports_functions()
    {
        $this->withoutErrorHandling();
        $response = $this->request('/using-exported-function.html', [
           TwigTemplateEngineFactory::CONFIG_KEY_FUNCTIONS => [
               'alien' => function (string $str): string {
                   return "👽 {$str}";
               },
           ],
        ]);
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertStringContainsString('<h1>This calls an 👽 function</h1>', $response->getContent());
    }
}
