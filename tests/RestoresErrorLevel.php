<?php

namespace Tests;

trait RestoresErrorLevel
{
    /**
     * @var int
     */
    private $originalErrorReportingLevel;

    /**
     * @before
     */
    protected function storeErrorReportingLevel()
    {
        $this->originalErrorReportingLevel = error_reporting();
    }

    /**
     * @after
     */
    protected function restoreErrorReportingLevel()
    {
        error_reporting($this->originalErrorReportingLevel);
    }
}
