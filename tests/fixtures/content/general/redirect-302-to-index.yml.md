---
title: Found Redirect 302
redirect: 
    location: /index.html
    code: 302
---
# Found (temporary) redirect