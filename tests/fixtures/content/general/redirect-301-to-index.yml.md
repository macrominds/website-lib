---
title: Permanent Redirect 301
redirect: 
    location: /
    code: 301
---
# Permanent redirect