<?php

namespace Tests;

use Macrominds\App\ContainerProvider;
use Macrominds\App\DefaultProjectPathProvider;
use Macrominds\App\ProjectPathProvider;
use Macrominds\BasicServiceProvider;
use Macrominds\Config\Config;
use Macrominds\Config\Env;
use Macrominds\Config\InvalidConfigurationException;
use Macrominds\ContentAccess\ContentAccess;
use Macrominds\ContentAccess\ContentPathProvider;
use Macrominds\Testing\ProvidesContainer;

class TestCase extends \PHPUnit\Framework\TestCase
{
    use ProvidesContainer;

    /**
     * @after
     */
    protected function tearDownErrorHandler(): void
    {
        $this->resetErrorHandler();
    }

    private function resetErrorHandler()
    {
        /* @noinspection PhpStatementHasEmptyBodyInspection */
        while (null !== set_error_handler(null)) {
            // set_error_handler null returns the previous exception handler or null if we are at the
            // initial state
        }
    }

    protected function getCachePath(): string
    {
        return $this->getProjectRoot('/cache');
    }

    /**
     * @param string $relPathLeadingSlash optional path, relative to the template root path. With leading slash.
     *                                    Example: '/my-relative-path'
     */
    protected function getTemplatePath(string $relPathLeadingSlash = ''): string
    {
        return $this->getFixturesPath("/templates{$relPathLeadingSlash}");
    }

    private function getFixturesPath(string $relPathLeadingSlash): string
    {
        return $this->getProjectRoot("/tests/fixtures{$relPathLeadingSlash}");
    }

    protected function getProjectRoot(string $relPathLeadingSlash = ''): string
    {
        return realpath(__DIR__.'/..').$relPathLeadingSlash;
    }

    protected function createAppServiceProvider(): BasicServiceProvider
    {
        return new BasicServiceProvider();
    }

    protected function getContentPath(string $relPathLeadingSlash): string
    {
        return $this->getFixturesPath("/content{$relPathLeadingSlash}");
    }

    protected function getContentPathProviderForAbsolutePath(string $absolutePath): ContentPathProvider
    {
        // TODO use getContainer()->resolve(...)
        $config = $this->getContainer()->resolve(Config::class);
        $config->set(
            ContentPathProvider::CONFIG_KEY_PATH_CONTENT, $absolutePath
        );

        return new ContentPathProvider($config);
    }

    protected function getContentPathProviderForRelativePath(string $relativePathLeadingSlash = '/general'): ContentPathProvider
    {
        return $this->getContentPathProviderForAbsolutePath($this->getContentPath($relativePathLeadingSlash));
    }

    /**
     * @throws InvalidConfigurationException
     */
    protected function getContentAccessForAbsolutePath(string $absolutePath): ContentAccess
    {
        return new ContentAccess($this->getContentPathProviderForAbsolutePath($absolutePath));
    }

    /**
     * @throws InvalidConfigurationException
     */
    protected function getContentAccessForRelativePath(string $relativePathLeadingSlash = '/general'): ContentAccess
    {
        return new ContentAccess($this->getContentPathProviderForRelativePath($relativePathLeadingSlash));
    }

    protected function getDotEnvTestingPath(): string
    {
        return '/tests/fixtures/config/dotenv/.env.testing';
    }

    protected function getConfigPath(string $relPathLeadingSlash): string
    {
        return $this->getFixturesPath("/config{$relPathLeadingSlash}");
    }

    protected function indexedValueArrayToAssocDataProvider(array $indexedValueArray): array
    {
        return array_combine(
            $indexedValueArray,
            array_map(
                function ($item) {
                    return [$item];
                },
                $indexedValueArray
            )
        );
    }

    protected function initializeContainerProvider(): void
    {
        $container = ContainerProvider::getInstance();
        $container->singleton(
            Env::class,
            function (): Env {
                return new Env($this->getProjectRoot());
            }
        );
        $container->singleton(
            Config::class,
            function () {
                return new Config();
            }
        );
        $container->singleton(
            ProjectPathProvider::class,
            function (): ProjectPathProvider {
                return new DefaultProjectPathProvider($this->getProjectRoot());
            }
        );
    }
}
