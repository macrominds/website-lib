<?php

namespace Tests\Setup;

use Macrominds\Rendering\TemplateEngine\Twig\Builder;
use Macrominds\Rendering\TemplateEngine\Twig\Twig;

trait SetupTwigEngine
{
    abstract protected function getCachePath(): string;

    abstract protected function getTemplatePath(string $relPathLeadingSlash = ''): string;

    protected function setupTwigEngine(
        $relTemplatePathLeadingSlash = '/general',
        callable $extend = null
    ): Twig {
        $builder = (new Builder($this->getCachePath()))
            ->setTemplatePaths([$this->getTemplatePath($relTemplatePathLeadingSlash)]);
        if ($extend) {
            $extend($builder);
        }

        return $builder->build();
    }
}
