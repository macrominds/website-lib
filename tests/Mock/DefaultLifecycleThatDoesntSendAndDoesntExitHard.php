<?php

namespace Tests\Mock;

use Macrominds\Lifecycle\DefaultLifecycle;
use Symfony\Component\HttpFoundation\Response;

class DefaultLifecycleThatDoesntSendAndDoesntExitHard extends DefaultLifecycle
{
    protected function sendResponse(Response $response): Response
    {
        // We're not sending anything inside the test

        return $response;
    }

    protected function shutdown(Response $response, callable $onShutdown = null)
    {
        if ($onShutdown) {
            $onShutdown($response);
        }
        // the original class runs `exit` – we don't want that in tests
    }
}
