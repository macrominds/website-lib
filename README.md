# macrominds website-lib

Use this simple framework to create websites with:

* [Twig Templates](https://twig.symfony.com/)
* [YAML content meta data](https://en.wikipedia.org/wiki/YAML)
* [Markdown content](https://en.wikipedia.org/wiki/Markdown)

The framework is designed with flexibility in mind, so that it might 
support other templating engines, meta data and content languages
later on. The idea is to be able to replace YAML with json, 
Markdown with Asciidoctor, Twig with blade and so on.

See the [Documentation](https://website.uber.space) for setup, usage and customization. 

## Extending and customization

You can replace the default services 
(see [\Macrominds\AppServiceProvider](src/AppServiceProvider.php)).

You do this by binding either singletons or multi object factories to the container.
This way, you can override the settings or add new bindings.

```php
use Macrominds\App;
use Macrominds\Processing\TemplateEngine\TemplateEngine;
use Symfony\Component\HttpFoundation\Request;

$app = new App(realpath(__DIR__ . '/..'));
$app->getContainer()->singleton(
    TemplateEngine::class,
    function(): TemplateEngine {
        return new MyVeryOwnTemplateEngine();
    }
);
$app->getContainer()->factoryObject(
    Request::class,
    function(): Request {
        return new MyCustomizedRequest(/*…*/);
    }
);

$app->getContainer()->resolve(Request::class)->myCustomRequestMethod();
```

An example usage is to manually debug a certain Request:

```php
use Macrominds\App;
use Symfony\Component\HttpFoundation\Request;

$app = new App(realpath(__DIR__ . '/..'));

// …

// always request /404.html
$app->getContainer()->factoryObject(Request::class, function() {
    return Request::create('/404.html');
});
```

## Testing this project

Run `./vendor/bin/phpunit` to run the tests.

To create a code coverage report, run `./code-coverage-report.sh`. 
You can then visit [report/coverage/index.html](report/coverage/index.html) to view the report.

## Automatic testing and docker images

This project provides a [.gitlab-ci.yml](.gitlab-ci.yml) and thus uses GitLab CI/CD.

The docker images inherit [macrominds/app php.df](https://gitlab.com/macrominds/app/-/blob/develop/ci/docker/php.df). Both docker files use build args for composer- and php versions. 

To build the docker images locally:

```bash
$ ./ci.bash build-all-local
``` 

Run phpunit inside the container:

```bash
$ docker run -it --rm -v $(pwd):/var/www/html macrominds/website-lib:php73 vendor/bin/phpunit
$ docker run -it --rm -v $(pwd):/var/www/html macrominds/website-lib:php74 vendor/bin/phpunit
$ docker run -it --rm -v $(pwd):/var/www/html macrominds/website-lib:php80 vendor/bin/phpunit
```

Build and publish the images for gitlab CI 
([also see gitlab docs](https://docs.gitlab.com/ee/user/packages/container_registry/index.html)):

```bash
$ ./ci.bash build-all-remotes-and-publish
```

## Code quality

Measure the current code quality with phpmetrics: 

```bash
$ ./phpmetrics-report.sh
```

## TODO

* Support symfony routes for more sophisticated websites and add individual resource route dynamically when it is requested
* Instead of using `<div class="block" markdown="1">` for blocks, it would be better 
  if we would just allow multiple markdown sections in a content file (for example separated by `---`)
* See if we can get rid of the standard IntlExtension in `Macrominds\TemplateEngine\Builder` and make it optional. 
  It requires "ext-intl" and is not needed for every website. 
* Observe the lack of activity at [erusev/parsedown](https://github.com/erusev/parsedown/pulls) and 
  [erusev/parsedown-extra](https://github.com/erusev/parsedown-extra/pulls) and 
  decide if we should switch to another lib. 
  [thephpleague/commonmark](https://github.com/thephpleague/commonmark) looks like a promising alternative. 
