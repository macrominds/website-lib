#!/usr/bin/env bash

# exit when a command fails
set -o errexit
# return the exit status of the last command that threw a non-zero exit code
set -o pipefail
# exit when script tries to use undeclared variables
set -o nounset

DOCKER_CMD=${DOCKER_CMD:-docker}
GITLAB_REGISTRY_PREFIX=${GITLAB_REGISTRY_PREFIX:-"registry.gitlab.com"}
IMAGE_BASE_NAME=${IMAGE_BASE_NAME:-"macrominds/website-lib"}
PHP_VERSION_ALIASES=("php80" "php81" "php82")

function build-all-local() {
  build-all "${IMAGE_BASE_NAME}"
}

function build-all() {
  local image_base_name="${1}"
  for php_version_alias in "${PHP_VERSION_ALIASES[@]}"; do
    ${DOCKER_CMD} build -t "${image_base_name}:${php_version_alias}" \
      --build-arg PHP_VERSION_ALIAS="${php_version_alias}" \
      --file ci/docker/php.df \
      ci/docker
  done
}

function build-all-remotes-and-publish() {
  build-all "${GITLAB_REGISTRY_PREFIX}/${IMAGE_BASE_NAME}"
  publish-all "${GITLAB_REGISTRY_PREFIX}/${IMAGE_BASE_NAME}"
}

function publish-all() {
  local image_base_name="${1}"
  ${DOCKER_CMD} login "${GITLAB_REGISTRY_PREFIX}"
  for php_version_alias in "${PHP_VERSION_ALIASES[@]}"; do
    ${DOCKER_CMD} push "${image_base_name}:${php_version_alias}"
  done
}

function test-all-local() {
  for php_version_alias in "${PHP_VERSION_ALIASES[@]}"; do
    test-local "${php_version_alias}"
  done
}

function test-local() {
  local php_version_alias="${1}"
  "${DOCKER_CMD}" run -it --rm -v "$(pwd)":/var/www/html "${IMAGE_BASE_NAME}:${php_version_alias}" vendor/bin/phpunit
}

# TODO print usage
$1 "${@:2}"
