<?php

namespace Macrominds\Routing;

use Macrominds\ContentAccess\ContentAccess;
use Macrominds\ContentAccess\ContentPathDoesNotExistException;
use Macrominds\Request\ParsedRequest;

class ResourceRouter implements Router
{
    /**
     * @var ContentAccess
     */
    private $contentAccess;
    /**
     * @var string
     */
    private $contentFileSuffix;

    public function __construct(ContentAccess $contentAccess, string $contentFileSuffix)
    {
        $this->contentAccess = $contentAccess;
        $this->contentFileSuffix = $contentFileSuffix;
    }

    /**
     * @throws ContentPathDoesNotExistException if neither the requested path, nor 404.html can be found
     */
    public function route(ParsedRequest $request): string
    {
        return $this->contentAccess->findExistingMappedPath($request->getPathInfo(), $this->contentFileSuffix);
    }
}
