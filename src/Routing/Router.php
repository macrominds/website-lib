<?php

namespace Macrominds\Routing;

use Macrominds\Request\ParsedRequest;

interface Router
{
    public function route(ParsedRequest $request): string;
}
