<?php

namespace Macrominds\Resource;

use Macrominds\Helpers\Arr;
use Macrominds\Rendering\TemplateEngine\Context;

class ContentResource
{
    /**
     * @var string
     */
    private $id;
    /**
     * @var string
     */
    private $type;
    /**
     * @var Context
     */
    private $context;

    /**
     * Resource constructor.
     *
     * @param string $id   e.g. '/index.yml.md'
     * @param string $type e.g. 'html'
     */
    public function __construct(string $id, string $type, Context $context = null)
    {
        $this->id = $id;
        $this->type = $type;
        $this->context = $context ?? new Context();
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function getFallbackType(): ?string
    {
        return $this->context['fallback_type'];
    }

    public function setContext(Context $context): self
    {
        $this->context = $context;

        return $this;
    }

    public function getContext(): Context
    {
        return $this->context;
    }

    public function mergeContextData(array $data): self
    {
        $this->setContext(
            new Context(
                Arr::replaceRecursive(
                    $this->getContext()->toArray(),
                    $data
                )
            )
        );

        return $this;
    }
}
