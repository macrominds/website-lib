<?php

namespace Macrominds\Resource;

use Macrominds\Config\CommonConfig;
use Macrominds\ContentAccess\ContentAccess;
use Macrominds\ContentAccess\ContentPathDoesNotExistException;
use Throwable;

class ErrorResourceFactory
{
    /**
     * @var ContentAccess
     */
    private $contentAccess;
    /**
     * @var CommonConfig
     */
    private $commonConfig;
    /**
     * @var bool
     */
    private $exposeErrorMessage;

    /**
     * ErrorResponseFactory constructor.
     */
    public function __construct(ContentAccess $contentAccess, CommonConfig $commonConfig, bool $exposeErrorMessage)
    {
        $this->contentAccess = $contentAccess;
        $this->commonConfig = $commonConfig;
        $this->exposeErrorMessage = $exposeErrorMessage;
    }

    /**
     * @throws ContentPathDoesNotExistException
     */
    public function createErrorResource(Throwable $e): ContentResource
    {
        $errorPage = $this->mapExceptionToPagePath($e);
        try {
            return $this->createContentResource($errorPage, $e);
        } catch (ContentPathDoesNotExistException $c) {
            $internalServerErrorPagePath = $this->getInternalServerErrorPagePath();
            if ($errorPage === $internalServerErrorPagePath) {
                // TODO wrap $c in custom exception
                throw $c;
            }

            return $this->createContentResource($internalServerErrorPagePath, $c);
        }
    }

    private function mapExceptionToPagePath(Throwable $e): string
    {
        // TODO make configurable, register ExceptionHandlers
        if ($e instanceof ContentPathDoesNotExistException) {
            return '404.html';
        }

        return $this->getInternalServerErrorPagePath();
    }

    private function getInternalServerErrorPagePath(): string
    {
        return '500.html';
    }

    /**
     * @throws ContentPathDoesNotExistException
     */
    private function createContentResource(string $pagePath, Throwable $e): ContentResource
    {
        return (new ContentResource(
            $this
                ->contentAccess
                ->findExistingMappedPath(
                    $pagePath,
                    $this->commonConfig->getContentFileSuffix()
                ),
            $this->commonConfig->getDefaultResponseType()
        ))->mergeContextData($this->translateToContextData($e));
    }

    private function translateToContextData(Throwable $e): array
    {
        if ($this->exposeErrorMessage) {
            return [
                'internal' => [
                    'err' => [
                        'no' => $e->getCode(),
                        'str' => $e->getMessage(),
                        'file' => $e->getFile(),
                        'line' => $e->getLine(),
                    ],
                ],
            ];
        }

        return [
            'internal' => [
                'err' => [
                    'str' => 'An error occurred. See logs.',
                ],
            ],
        ];
    }
}
