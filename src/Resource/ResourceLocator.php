<?php

namespace Macrominds\Resource;

use Macrominds\Config\CommonConfig;
use Macrominds\ContentAccess\ContentPathDoesNotExistException;
use Macrominds\Rendering\TemplateEngine\Context;
use Macrominds\Request\ParsedRequest;
use Macrominds\Routing\ResourceRouter;

class ResourceLocator
{
    /**
     * @var ResourceRouter
     */
    private $requestMapper;
    /**
     * @var CommonConfig
     */
    private $commonConfig;

    /**
     * ResourceLocator constructor.
     */
    public function __construct(ResourceRouter $requestMapper, CommonConfig $commonConfig)
    {
        $this->requestMapper = $requestMapper;
        $this->commonConfig = $commonConfig;
    }

    /**
     * @throws ContentPathDoesNotExistException
     */
    public function locateResource(ParsedRequest $rewrittenRequest): ContentResource
    {
        return new ContentResource(
            $this->locatePath($rewrittenRequest),
            $this->getRequestSuffix($rewrittenRequest),
            new Context([
                'requested_path' => $rewrittenRequest->getPathInfo(),
                'fallback_type' => $this->commonConfig->getDefaultResponseType(),
            ])
        );
    }

    /**
     * @throws ContentPathDoesNotExistException
     */
    private function locatePath(ParsedRequest $rewrittenRequest): string
    {
        return $this->requestMapper->route($rewrittenRequest);
    }

    private function getRequestSuffix(ParsedRequest $request): string
    {
        return $request->getSuffix($this->commonConfig->getDefaultResponseType());
    }
}
