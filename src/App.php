<?php

namespace Macrominds;

use Macrominds\App\App as MacromindsApp;
use Macrominds\App\HasDefaultProjectPath;
use Macrominds\Config\IsConfigurable;
use Macrominds\Config\UsesDotEnv;
use Macrominds\Lifecycle\AbstractLifecycle;
use Macrominds\Services\AllowsServiceRegistration;

class App extends MacromindsApp
{
    use UsesDotEnv;
    use IsConfigurable;
    use AllowsServiceRegistration;
    use HasDefaultProjectPath;

    public function run(callable $onShutdown = null)
    {
        $lifecycle = $this->getContainer()->resolve(AbstractLifecycle::class);
        $lifecycle->withErrorHandling();
        $lifecycle->run($onShutdown);
    }

    protected function registerServices(string $projectPath)
    {
        $this->registerDotEnv($projectPath);
        $this->registerDefaultConfig();
        $this->registerServiceProvider(new BasicServiceProvider());
        $this->registerProjectPathProvider($projectPath);
    }
}
