<?php

namespace Macrominds\Frontmatter;

use Macrominds\FrontmatterPageFactory;
use Macrominds\Page\PageFactory;
use Macrominds\Parser\DataParser;
use Macrominds\Rendering\ContentRenderer;
use Macrominds\Services\AppServiceProvider;
use Macrominds\Services\Container;

class FrontmatterServiceProvider extends AppServiceProvider
{
    public function getSingletonServices(Container $c): array
    {
        return [
            PageFactory::class => function () use ($c): PageFactory {
                return new FrontmatterPageFactory(
                    $c->resolve(DataParser::class),
                    $c->resolve(ContentRenderer::class),
                );
            },
        ];
    }

    public function getFactoryObjectServices(Container $c): array
    {
        return [];
    }
}
