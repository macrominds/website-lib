<?php

namespace Macrominds\Frontmatter;

use Macrominds\Frontmatter\FrontmatterParser\BlockLocation\BlockLocation;
use Macrominds\Frontmatter\FrontmatterParser\BlockLocation\BlockLocator;

class FrontmatterParser
{
    public function extract(string $string): array
    {
        $blockLocation = (new BlockLocator($string))->findBlock("---\n", "\n---\n");

        return $this->extractBlock($string, $blockLocation);
    }

    public function extractBlock(string $string, BlockLocation $location): array
    {
        $locationStart = $location->getStart();
        $locationEnd = $location->getEnd();
        $block = substr($string, $locationStart, $locationEnd - $locationStart);
        $remainder = substr($string, $locationEnd);

        return [$block, $remainder];
    }
}
