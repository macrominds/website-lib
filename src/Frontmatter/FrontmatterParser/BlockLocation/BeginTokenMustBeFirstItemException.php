<?php

namespace Macrominds\Frontmatter\FrontmatterParser\BlockLocation;

use Exception;

class BeginTokenMustBeFirstItemException extends Exception
{
    public function __construct(string $haystack, string $token)
    {
        parent::__construct("The string does not start with the token „{$token}“ as expected.".
            "The token must be on position 0, nothing shall precede it.\n${haystack}"
        );
    }
}
