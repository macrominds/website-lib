<?php

namespace Macrominds\Frontmatter\FrontmatterParser\BlockLocation;

class BlockLocator
{
    /**
     * @var string
     */
    private $completeContent;

    public function __construct(string $completeContent)
    {
        $this->completeContent = $completeContent;
    }

    public function findBlock(string $beginToken, string $endToken): BlockLocation
    {
        try {
            return $this->findBlockOrFail($beginToken, $endToken);
        } catch (BeginTokenMustBeFirstItemException | EndTokenNotFoundException $e) {
            return new MissingBlockLocation();
        }
    }

    /**
     * @throws BeginTokenMustBeFirstItemException
     * @throws EndTokenNotFoundException
     */
    private function findBlockOrFail(string $beginToken, string $endToken): BlockLocation
    {
        $beginIdx = $this->findBeginIdx($beginToken);
        $endIdx = $this->findEndIdx($beginToken, $beginIdx, $endToken);

        $endTokenLength = strlen($endToken);

        return new BlockLocation($beginIdx, $endIdx + $endTokenLength);
    }

    /**
     * @throws BeginTokenMustBeFirstItemException
     */
    private function findBeginIdx(string $beginToken)
    {
        $beginIdx = strpos($this->completeContent, $beginToken);
        if (0 !== $beginIdx) {
            throw new BeginTokenMustBeFirstItemException($this->completeContent, $beginToken);
        }

        return $beginIdx;
    }

    /**
     * @throws EndTokenNotFoundException
     */
    private function findEndIdx(string $beginToken, int $beginIdx, string $endToken)
    {
        $beginTokenLength = strlen($beginToken);
        $endIdx = strpos($this->completeContent, $endToken, $beginIdx + $beginTokenLength);
        if (false === $endIdx) {
            throw new EndTokenNotFoundException($this->completeContent, $beginToken, $endToken);
        }

        return $endIdx;
    }
}
