<?php

namespace Macrominds\Frontmatter\FrontmatterParser\BlockLocation;

class MissingBlockLocation extends BlockLocation
{
    public function __construct()
    {
        parent::__construct(0, 0);
    }
}
