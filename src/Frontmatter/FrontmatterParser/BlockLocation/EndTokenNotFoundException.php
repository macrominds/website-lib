<?php

namespace Macrominds\Frontmatter\FrontmatterParser\BlockLocation;

use Exception;

class EndTokenNotFoundException extends Exception
{
    public function __construct(string $haystack, string $beginToken, string $endToken)
    {
        parent::__construct("The end token „{$endToken}“ is missing after the begin token „{$beginToken}“.\n".
            "${haystack}"
        );
    }
}
