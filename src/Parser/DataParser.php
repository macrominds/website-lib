<?php

namespace Macrominds\Parser;

interface DataParser
{
    public function parse(string $raw): array;
}
