<?php

namespace Macrominds;

use Macrominds\Config\Config;
use Macrominds\Rendering\TemplateEngine\TemplateEngine;
use Macrominds\Rendering\TemplateEngine\TemplateEngineFactory;
use Macrominds\Rendering\TemplateEngine\Twig\TwigTemplateEngineFactory;
use Macrominds\Services\AppServiceProvider;
use Macrominds\Services\Container;

class TwigServiceProvider extends AppServiceProvider
{
    public function getSingletonServices(Container $c): array
    {
        return [
            TemplateEngine::class => function () use ($c): TemplateEngine {
                return $c->resolve(TemplateEngineFactory::class)->create();
            },
            TemplateEngineFactory::class => function () use ($c): TemplateEngineFactory {
                return new TwigTemplateEngineFactory($c->resolve(Config::class));
            },
        ];
    }

    public function getFactoryObjectServices(Container $c): array
    {
        return [];
    }
}
