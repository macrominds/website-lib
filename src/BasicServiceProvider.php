<?php

namespace Macrominds;

use Macrominds\App\ProjectPathProvider;
use Macrominds\Config\CommonConfig;
use Macrominds\Config\Config;
use Macrominds\Config\Env;
use Macrominds\ContentAccess\ContentAccess;
use Macrominds\ContentAccess\ContentPathProvider;
use Macrominds\Lifecycle\AbstractLifecycle;
use Macrominds\Lifecycle\DefaultLifecycle;
use Macrominds\Lifecycle\ResponseFactory;
use Macrominds\Logger\LoggerFactory;
use Macrominds\Request\Rewrite\RequestRewriterStrategy;
use Macrominds\Request\Rewrite\RootSlashToIndexRewriter;
use Macrominds\Resource\ErrorResourceFactory;
use Macrominds\Resource\ResourceLocator;
use Macrominds\Routing\ResourceRouter;
use Macrominds\Services\AppServiceProvider;
use Macrominds\Services\Container;
use Symfony\Component\HttpFoundation\Request;

class BasicServiceProvider extends AppServiceProvider
{
    public function getSingletonServices(Container $c): array
    {
        return [
            ProjectPathProvider::class => function (): ProjectPathProvider {
                throw new MissingProjectPathProviderException('Please provide a ProjectPathProvider');
            },
            Config::class => function () use ($c): Config {
                return new Config($c['config'] ?? []);
            },
            Env::class => function () use ($c): Env {
                return new Env($c->resolve(ProjectPathProvider::class)->getProjectPath());
            },
            CommonConfig::class => function () use ($c): CommonConfig {
                return new CommonConfig($c->resolve(Config::class));
            },
            LoggerFactory::class => function () use ($c): LoggerFactory {
                return new LoggerFactory($c->resolve(Config::class));
            },
            ResponseFactory::class => function (): ResponseFactory {
                return new ResponseFactory();
            },
            RequestRewriterStrategy::class => function (): RequestRewriterStrategy {
                // TODO use configurable RewriterChain
                return new RootSlashToIndexRewriter();
            },
            ContentAccess::class => function () use ($c): ContentAccess {
                return new ContentAccess($c->resolve(ContentPathProvider::class));
            },
            ContentPathProvider::class => function () use ($c): ContentPathProvider {
                return new ContentPathProvider(
                    $c->resolve(Config::class)
                );
            },
            ResourceRouter::class => function () use ($c): ResourceRouter {
                $contentFileSuffix = $c->resolve(CommonConfig::class)->getContentFileSuffix();

                return new ResourceRouter($c->resolve(ContentAccess::class), $contentFileSuffix);
            },
            ResourceLocator::class => function () use ($c): ResourceLocator {
                return new ResourceLocator(
                    $c->resolve(ResourceRouter::class),
                    $c->resolve(CommonConfig::class)
                );
            },
            ErrorResourceFactory::class => function () use ($c): ErrorResourceFactory {
                $commonConfig = $c->resolve(CommonConfig::class);

                return new ErrorResourceFactory(
                    $c->resolve(ContentAccess::class),
                    $commonConfig,
                    $commonConfig->isProduction(),
                );
            },
            AbstractLifecycle::class => function () use ($c): AbstractLifecycle {
                return new DefaultLifecycle($c);
            },
        ];
    }

    public function getFactoryObjectServices(Container $c): array
    {
        return [
            Request::class => $c->factory(function (): Request {
                return Request::createFromGlobals();
            }),
        ];
    }
}
