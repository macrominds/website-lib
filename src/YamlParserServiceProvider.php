<?php

namespace Macrominds;

use Macrominds\Parser\DataParser;
use Macrominds\Services\AppServiceProvider;
use Macrominds\Services\Container;
use Macrominds\YamlParser\YamlParserFactory;

class YamlParserServiceProvider extends AppServiceProvider
{
    public function getSingletonServices(Container $c): array
    {
        return [
            DataParser::class => function () use ($c) {
                return (new YamlParserFactory($c))->createYamlParser();
            },
        ];
    }

    public function getFactoryObjectServices(Container $c): array
    {
        return [
        ];
    }
}
