<?php

namespace Macrominds\Lifecycle;

use Macrominds\Page\Page;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;

class ResponseFactory
{
    public function createResponseFor(Page $page): Response
    {
        if ($page->data('redirect.location')) {
            // TODO use RedirectResponseFactory
            return new RedirectResponse(
                $page->data('redirect.location'),
                // 302 default represents symfony RedirectResponse default
                $page->data('redirect.code', 302)
            );
        }

        return new Response($page->getContent(), $page->data('status_code', 200));
    }
}
