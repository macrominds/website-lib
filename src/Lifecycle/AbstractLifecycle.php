<?php

namespace Macrominds\Lifecycle;

use Macrominds\ContentAccess\ContentPathDoesNotExistException;
use Macrominds\Page\Page;
use Macrominds\Request\ParsedRequest;
use Macrominds\Resource\ContentResource;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Throwable;

abstract class AbstractLifecycle
{
    use HandleErrors;

    /**
     * @param callable|null $onShutdown
     */
    private $onShutdown = null;

    /**
     * @throws Throwable
     */
    final public function run(callable $onShutdown = null)
    {
        $this->onShutdown = $onShutdown;
        $parsedRequest = null;
        try {
            $this->boot();
            $request = $this->fetchRequest();
            $parsedRequest = $this->parseRequest($request);
            $rewrittenRequest = $this->rewriteRequest($parsedRequest);
            $response = $this->getResponse($rewrittenRequest);
        } catch (Throwable $e) {
            if ($this->hasErrorHandling()) {
                $this->exceptionHandler($e);

                return;
            }
            throw $e;
        }
        $response = $this->sendResponse($response);
        $this->shutdown($response, $this->onShutdown);
    }

    abstract protected function boot();

    abstract protected function fetchRequest(): Request;

    abstract protected function parseRequest(Request $request): ParsedRequest;

    abstract protected function rewriteRequest(ParsedRequest $parsedRequest): ParsedRequest;

    /**
     * @throws ContentPathDoesNotExistException
     */
    final protected function getResponse(ParsedRequest $rewrittenRequest): Response
    {
        return $this->getResponseForResource(
            $this->locateResource($rewrittenRequest)
        );
    }

    final protected function getResponseForResource(ContentResource $resource): Response
    {
        return $this->createResponse(
            $this->createPage($resource)
        );
    }

    abstract protected function createErrorResponse(Throwable $e): Response;

    /**
     * @throws ContentPathDoesNotExistException
     */
    abstract protected function locateResource(ParsedRequest $rewrittenRequest): ContentResource;

    abstract protected function createPage(ContentResource $resource): Page;

    abstract protected function createResponse(Page $page): Response;

    abstract protected function sendResponse(Response $response): Response;

    abstract protected function shutdown(Response $response, callable $onShutdown = null);
}
