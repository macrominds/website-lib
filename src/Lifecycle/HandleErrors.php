<?php

namespace Macrominds\Lifecycle;

use Throwable;

trait HandleErrors
{
    /**
     * @var bool
     */
    private $errorHandling = true;

    final public function withErrorHandling(): void
    {
        $this->errorHandling = true;
        set_error_handler([$this, 'errorHandler']);
    }

    final public function withoutErrorHandling(): void
    {
        $this->errorHandling = false;
        /* @noinspection PhpStatementHasEmptyBodyInspection */
        while (null !== set_error_handler(null)) {
            // set_error_handler null returns the previous exception handler or null if we are at the
            // initial state
        }
    }

    final public function hasErrorHandling(): bool
    {
        return $this->errorHandling;
    }

    /**
     * @throws HandledErrorException
     */
    public function errorHandler(int $errno, string $errstr, string $errfile, int $errline)
    {
        throw new HandledErrorException($errstr, $errno, $errfile, $errline);
    }

    final protected function exceptionHandler(Throwable $e): void
    {
        $response = $this->createErrorResponse($e);
        $response = $this->sendResponse($response);
        $this->shutdown($response, $this->onShutdown);
    }
}
