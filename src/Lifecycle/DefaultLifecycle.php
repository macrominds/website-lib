<?php

namespace Macrominds\Lifecycle;

use Macrominds\App\HasContainer;
use Macrominds\Config\CommonConfig;
use Macrominds\Config\InvalidConfigurationException;
use Macrominds\ContentAccess\ContentPathDoesNotExistException;
use Macrominds\ContentAccess\ContentPathProvider;
use Macrominds\Logger\LoggerFactory;
use Macrominds\Page\Page;
use Macrominds\Page\PageFactory;
use Macrominds\Rendering\Renderer;
use Macrominds\Rendering\TemplateEngine\TemplateEngine;
use Macrominds\Rendering\TemplateEngine\TemplateNotFoundException;
use Macrominds\Request\ParsedRequest;
use Macrominds\Request\Rewrite\RequestRewriterStrategy;
use Macrominds\Resource\ContentResource;
use Macrominds\Resource\ErrorResourceFactory;
use Macrominds\Resource\ResourceLocator;
use Macrominds\Services\Container;
use Monolog\Logger;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Throwable;

class DefaultLifecycle extends AbstractLifecycle
{
    use HasContainer;

    /**
     * @var Logger
     */
    private $logger;

    public function __construct(Container $container = null)
    {
        $this->container = $this->provideContainer($container);
        $this->withErrorHandling();
    }

    protected function boot()
    {
    }

    protected function fetchRequest(): Request
    {
        return $this->container->resolve(Request::class);
    }

    protected function parseRequest(Request $request): ParsedRequest
    {
        return new ParsedRequest($request);
    }

    protected function rewriteRequest(ParsedRequest $parsedRequest): ParsedRequest
    {
        return $this->container->resolve(RequestRewriterStrategy::class)->rewriteRequestPath($parsedRequest);
    }

    /**
     * @throws ContentPathDoesNotExistException
     * @throws InvalidConfigurationException
     */
    protected function locateResource(ParsedRequest $rewrittenRequest): ContentResource
    {
        return $this->container->resolve(ResourceLocator::class)->locateResource($rewrittenRequest);
    }

    /**
     * @throws InvalidConfigurationException
     */
    protected function createPage(ContentResource $resource): Page
    {
        /**
         * @var Page
         */
        $page = $this->container->resolve(PageFactory::class)->createFrom($resource);
        // TODO handle exceptions ?!
        return $this->renderPage($page, $resource);
    }

    /**
     * @throws ContentPathDoesNotExistException
     * @throws InvalidConfigurationException
     * @throws TemplateNotFoundException
     */
    private function renderPage(Page $page, ContentResource $resource): Page
    {
        return $this->createPageRenderer()->renderPage(
            array_merge($page->getData(), [Renderer::KEY_DATA_CONTENT => $page->getContent()]),
            $resource,
        );
    }

    private function createPageRenderer(): Renderer
    {
        return new Renderer(
            $this->container->resolve(ContentPathProvider::class),
            $this->container->resolve(TemplateEngine::class)
        );
    }

    protected function createResponse(Page $page): Response
    {
        return $this->container->resolve(ResponseFactory::class)->createResponseFor($page);
    }

    /**
     * @codeCoverageIgnore
     */
    protected function sendResponse(Response $response): Response
    {
        return $response->send();
    }

    /**
     * @codeCoverageIgnore
     */
    protected function shutdown(Response $response, callable $onShutdown = null)
    {
        if ($onShutdown) {
            $onShutdown($response);
        }
        exit;
    }

    protected function createErrorResponse(Throwable $e): Response
    {
        try {
            $this->logger()->error($e);

            return $this->getResponseForResource(
                $this->container->resolve(ErrorResourceFactory::class)->createErrorResource($e)
            );
        } catch (Throwable $e) {
            $this->tryLoggingOrSwallowException($e);

            return $this->createFatalErrorResponse($e);
        }
    }

    private function tryLoggingOrSwallowException(Throwable $e): void
    {
        try {
            $this->logger()->error($e);
        } catch (Throwable $e) {
            // swallow Exception, this was just an attempt to log, but the logger instantiation itself failed
        }
    }

    private function logger(): Logger
    {
        // TODO Let the container manage the singleton
        if (! $this->logger) {
            /* @noinspection PhpUnhandledExceptionInspection because this Exception is thrown at most once */
            $this->logger = $this->container->resolve(LoggerFactory::class)->createLogger('mwf');
        }

        return $this->logger;
    }

    protected function createFatalErrorResponse(Throwable $e)
    {
        $message = '';
        if ($this->exposeErrorMessage()) {
            $message = $e->getMessage();
        }

        return new Response($message, 500);
    }

    private function exposeErrorMessage(): bool
    {
        return $this->container->resolve(CommonConfig::class)->isProduction();
    }
}
