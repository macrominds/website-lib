<?php

namespace Macrominds\Logger;

use InvalidArgumentException;
use Macrominds\Config\Config;
use Macrominds\Config\InvalidConfigurationException;
use Monolog\Formatter\FormatterInterface;
use Monolog\Formatter\LineFormatter;
use Monolog\Handler\Handler;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;

class LoggerFactory
{
    public const CONFIG_KEY_PATH_LOGGING = 'path.logging';
    /**
     * @var Config
     */
    private $config;

    public function __construct(Config $config)
    {
        $this->config = $config;
    }

    /**
     * @throws InvalidConfigurationException
     */
    public function createLogger(string $name)
    {
        $logger = new Logger($name);
        try {
            $logger->pushHandler($this->createLogStreamHandler($this->getConfig(), $this->createLogFormatter()));
        } catch (InvalidArgumentException $e) {
            throw new InvalidConfigurationException(self::CONFIG_KEY_PATH_LOGGING, 'Cannot create a logger without a validly configured logfile location.', 0, $e);
        }

        return $logger;
    }

    protected function createLogStreamHandler(Config $config, FormatterInterface $formatter): Handler
    {
        $handler = new StreamHandler($config->get(self::CONFIG_KEY_PATH_LOGGING), Logger::ERROR);
        $handler->setFormatter($formatter);

        return $handler;
    }

    protected function createLogFormatter(): FormatterInterface
    {
        $lineFormatter = new LineFormatter();
        $lineFormatter->includeStacktraces();

        return $lineFormatter;
    }

    public function getConfig(): Config
    {
        return $this->config;
    }
}
