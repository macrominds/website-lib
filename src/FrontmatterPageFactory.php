<?php

namespace Macrominds;

use Macrominds\ContentAccess\ContentPathDoesNotExistException;
use Macrominds\Frontmatter\FrontmatterParser;
use Macrominds\Page\Page;
use Macrominds\Page\PageFactory;
use Macrominds\Parser\DataParser;
use Macrominds\Rendering\ContentRenderer;
use Macrominds\Rendering\TemplateEngine\TemplateNotFoundException;
use Macrominds\Resource\ContentResource;

class FrontmatterPageFactory implements PageFactory
{
    /**
     * @var DataParser
     */
    private $parser;
    /**
     * @var ContentRenderer
     */
    private $contentRenderer;

    public function __construct(DataParser $parser, ContentRenderer $contentRenderer)
    {
        $this->parser = $parser;
        $this->contentRenderer = $contentRenderer;
    }

    /**
     * @throws ContentPathDoesNotExistException
     * @throws TemplateNotFoundException
     */
    final public function createFrom(ContentResource $resource): Page
    {
        $rawContent = $this->readRaw($resource->getId());
        [$data, $content] = $this->extractFrontmatter($rawContent);

        return $this->createPageFrom($data, $content);
    }

    private function readRaw(string $path): string
    {
        return file_get_contents($path);
    }

    private function extractFrontmatter(string $rawContent): array
    {
        return (new FrontmatterParser())->extract($rawContent);
    }

    private function createPageFrom(string $data, string $content): Page
    {
        return new Page(
        // TODO: Should we postpone rendering to a Postprocessor
        // (that is, render markdown after twig)?
            $this->contentRenderer->render($content),
            $this->parser->parse($data)
        );
    }
}
