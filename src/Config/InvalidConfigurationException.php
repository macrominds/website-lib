<?php

namespace Macrominds\Config;

use Exception;
use Throwable;

class InvalidConfigurationException extends Exception
{
    /**
     * @var string
     */
    private $invalidConfigurationKey;

    public function __construct(
        string $missingConfigurationKey,
        string $message = null,
        int $code = 0,
        Throwable $previous = null
    ) {
        parent::__construct("Configuration '{$missingConfigurationKey}' missing or invalid. {$message}", $code, $previous);
        $this->invalidConfigurationKey = $missingConfigurationKey;
    }

    public function getInvalidConfigurationKey(): string
    {
        return $this->invalidConfigurationKey;
    }
}
