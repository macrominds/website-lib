<?php

namespace Macrominds\Config;

class CommonConfig
{
    const KEY_SUFFIX_CONTENT_FILE = 'content-file-suffix';
    const KEY_DEFAULT_RESPONSE_TYPE = 'default-response-type';
    const KEY_APP_ENV = 'APP_ENV';

    /**
     * @var Config
     */
    private $config;

    public function __construct(Config $config)
    {
        $this->config = $config;
    }

    public function getContentFileSuffix(): string
    {
        return $this->config->get(self::KEY_SUFFIX_CONTENT_FILE, 'yml.md');
    }

    public function getDefaultResponseType(): string
    {
        return $this->config->get(self::KEY_DEFAULT_RESPONSE_TYPE, 'html');
    }

    public function isProduction(): string
    {
        return 'production' !== $this->config->get(self::KEY_APP_ENV, 'production');
    }
}
