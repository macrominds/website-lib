<?php

namespace Macrominds\Request\Rewrite;

use Macrominds\Request\ParsedRequest;

class RootSlashToIndexRewriter implements RequestRewriterStrategy
{
    public function rewriteRequestPath(ParsedRequest $request): ParsedRequest
    {
        if ('/' === $request->getPathInfo()) {
            $request->setPathInfo('/index.html');
        }

        return $request;
    }
}
