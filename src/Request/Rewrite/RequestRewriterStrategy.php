<?php

namespace Macrominds\Request\Rewrite;

use Macrominds\Request\ParsedRequest;

interface RequestRewriterStrategy
{
    public function rewriteRequestPath(ParsedRequest $request): ParsedRequest;
}
