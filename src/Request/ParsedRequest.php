<?php

namespace Macrominds\Request;

use Macrominds\Helpers\PathInfo;
use Symfony\Component\HttpFoundation\Request;

class ParsedRequest
{
    /**
     * @var string
     */
    private $pathInfo;

    public function __construct(Request $delegate)
    {
        $this->setPathInfo($delegate->getPathInfo());
    }

    public function setPathInfo(string $pathInfo): ParsedRequest
    {
        $this->pathInfo = $pathInfo;

        return $this;
    }

    public function getPathInfo(): string
    {
        return $this->pathInfo;
    }

    public function getSuffix(string $fallback): string
    {
        $p = new PathInfo($this->pathInfo);

        return $p->getSuffix() ?? $fallback;
    }
}
