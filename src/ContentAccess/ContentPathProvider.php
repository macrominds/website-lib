<?php

namespace Macrominds\ContentAccess;

use Macrominds\Config\Config;
use Macrominds\Config\InvalidConfigurationException;

class ContentPathProvider
{
    public const CONFIG_KEY_PATH_CONTENT = 'path.content';

    /**
     * @var Config
     */
    private $config;

    public function __construct(Config $config)
    {
        $this->config = $config;
    }

    /**
     * @throws InvalidConfigurationException
     */
    public function getValidatedContentPath(): string
    {
        $path = $this->config->get(self::CONFIG_KEY_PATH_CONTENT);
        if (null === $path) {
            throw new InvalidConfigurationException(self::CONFIG_KEY_PATH_CONTENT, 'Please configure the path to your content files. 
                      E.g. [\'ContentPathProvider::CONFIG_KEY_PATH_CONTENT\' => projectPath(\'/content\')].');
        }

        return $path;
    }
}
