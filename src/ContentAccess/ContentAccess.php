<?php

namespace Macrominds\ContentAccess;

use Macrominds\Config\InvalidConfigurationException;
use Macrominds\Helpers\PathInfo;

class ContentAccess
{
    /**
     * @var string
     */
    private $path;

    /**
     * @throws InvalidConfigurationException
     */
    public function __construct(ContentPathProvider $contentPathProvider)
    {
        $this->path = $contentPathProvider->getValidatedContentPath();
    }

    public function getPath(): string
    {
        return $this->path;
    }

    /**
     * @throws ContentPathDoesNotExistException
     */
    public function findExistingMappedPath(string $source, string $targetSuffix): string
    {
        $pathInfo = new PathInfo($source);
        $prefix = $pathInfo->getFilenameWithoutSuffix();
        $filename = "{$prefix}.{$targetSuffix}";

        return $this->findExisting($filename);
    }

    /**
     * @throws ContentPathDoesNotExistException
     */
    public function findExisting(string $filename): string
    {
        $realPath = realpath("{$this->path}/{$filename}");
        if (false !== $realPath && is_file($realPath) && $this->isBelowContentPath($realPath)) {
            return $realPath;
        }
        throw new ContentPathDoesNotExistException($filename, $this->path);
    }

    public function isBelowContentPath(string $realPath): bool
    {
        // TODO refactor: Extract PathValidator or PathGuard and make it extendable instead
        return 0 === strpos($realPath, $this->getPath());
    }
}
