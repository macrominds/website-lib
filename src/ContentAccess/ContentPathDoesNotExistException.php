<?php

namespace Macrominds\ContentAccess;

use Exception;

class ContentPathDoesNotExistException extends Exception
{
    /**
     * @var string
     */
    private $path;
    /**
     * @var string
     */
    private $contentDir;

    public function __construct(?string $path, string $contentDir)
    {
        $this->path = $path;
        $this->contentDir = $contentDir;
        $message = ($path ?? 'null-path').' not found in '.$contentDir.'.';
        parent::__construct($message);
    }

    public function getPath()
    {
        return $this->path;
    }

    public function getContentDir(): string
    {
        return $this->contentDir;
    }
}
