<?php

namespace Macrominds\Rendering;

use Macrominds\Config\InvalidConfigurationException;
use Macrominds\ContentAccess\ContentPathDoesNotExistException;
use Macrominds\ContentAccess\ContentPathProvider;
use Macrominds\Helpers\Arr;
use Macrominds\Page\Page;
use Macrominds\Rendering\TemplateEngine\Context;
use Macrominds\Rendering\TemplateEngine\TemplateEngine;
use Macrominds\Rendering\TemplateEngine\TemplateNotFoundException;
use Macrominds\Resource\ContentResource;

class Renderer
{
    public const KEY_DATA_CONTENT = 'content';
    /**
     * @var TemplateEngine
     */
    private $templateEngine;
    /**
     * @var ContentPathProvider
     */
    private $contentPathProvider;

    /**
     * Renderer constructor.
     */
    public function __construct(ContentPathProvider $contentPathProvider, TemplateEngine $templateEngine)
    {
        $this->contentPathProvider = $contentPathProvider;
        $this->templateEngine = $templateEngine;
    }

    /**
     * @throws ContentPathDoesNotExistException
     * @throws InvalidConfigurationException
     * @throws TemplateNotFoundException
     */
    public function renderPage(array $data, ContentResource $resource): Page
    {
        $data = $this->mergeDataWithResourceContext($data, $resource->getContext());
        $data[self::KEY_DATA_CONTENT] = $this->renderContentOrFail($data, $resource);

        return new Page($data[self::KEY_DATA_CONTENT], $data);
    }

    protected function mergeDataWithResourceContext(array $data, Context $resourceContext): array
    {
        return Arr::replaceRecursive($data, $resourceContext->toArray());
    }

    /**
     * @throws ContentPathDoesNotExistException
     * @throws TemplateNotFoundException
     * @throws InvalidConfigurationException
     */
    protected function renderContentOrFail(array $data, ContentResource $resource): string
    {
        try {
            return $this->renderContent($data, $resource->getType());
        } catch (TemplateNotFoundException $e) {
            throw $this->mapRenderException($e, $data, $resource->getFallbackType());
        }
    }

    /**
     * @throws TemplateNotFoundException
     */
    protected function renderContent(array $data, string $type): string
    {
        return $this->templateEngine->render(
            new Context($data),
            $type
        );
    }

    /**
     *@throws InvalidConfigurationException
     *
     * @return ContentPathDoesNotExistException|TemplateNotFoundException
     */
    protected function mapRenderException(TemplateNotFoundException $e, array $data, string $type)
    {
        if ($this->existsFallback(new Context($data), $type)) {
            // TODO: What about the path?
            return new ContentPathDoesNotExistException(null, $this->getContentDir());
        }

        return $e;
    }

    /**
     * @throws InvalidConfigurationException
     */
    protected function getContentDir(): string
    {
        return $this->contentPathProvider->getValidatedContentPath();
    }

    protected function existsFallback(Context $context, string $type): bool
    {
        // render temporarily with fallback type.
        // It might be more elegant to find the template, but this would require us to use internal API if
        // we're using Twig.
        try {
            $this->templateEngine->render($context, $type);

            return true;
        } catch (TemplateNotFoundException $e) {
            return false;
        }
    }
}
