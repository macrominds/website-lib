<?php

namespace Macrominds\Rendering\TemplateEngine;

use Exception;
use Throwable;

class TemplateNotFoundException extends Exception
{
    /**
     * @var string
     */
    private $name;

    public function __construct(?string $name, Throwable $previous = null)
    {
        $this->name = $name;
        $message = 'Unable to find template "'.($name ?? 'null-path').'"';
        parent::__construct($message, 0, $previous);
    }
}
