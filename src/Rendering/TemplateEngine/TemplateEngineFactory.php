<?php

namespace Macrominds\Rendering\TemplateEngine;

use Macrominds\Config\InvalidConfigurationException;

interface TemplateEngineFactory
{
    public const CONFIG_KEY_PATH_CACHE = 'path.cache';
    // TODO check this: There's a path templates and a path template!
    public const CONFIG_KEY_PATH_TEMPLATE = 'path.template';

    /**
     * @throws InvalidConfigurationException
     */
    public function create(): TemplateEngine;
}
