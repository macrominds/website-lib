<?php

namespace Macrominds\Rendering\TemplateEngine\Twig;

use Macrominds\Config\Config;
use Macrominds\Config\InvalidConfigurationException;
use Macrominds\Rendering\TemplateEngine\TemplateEngine;
use Macrominds\Rendering\TemplateEngine\TemplateEngineFactory;

class TwigTemplateEngineFactory implements TemplateEngineFactory
{
    const CONFIG_KEY_DEBUG_MODE = 'template-engine.debug-mode';
    const CONFIG_KEY_FUNCTIONS = 'template-engine.exported.functions';
    // TODO check this: There's a path templates and a path template!
    const CONFIG_KEY_PATH_TEMPLATES = 'path.templates';

    /**
     * @var Config
     */
    private $config;

    public function __construct(Config $config)
    {
        $this->config = $config;
    }

    /**
     * @throws InvalidConfigurationException
     */
    public function create(): TemplateEngine
    {
        $builder = (new Builder($this->validatedPathCacheConfig()))
            ->setDebugMode($this->config->get(self::CONFIG_KEY_DEBUG_MODE, false))
            ->setTemplatePaths($this->validatedPathTemplatesConfig());

        $exportedFunctions = $this->config->get(self::CONFIG_KEY_FUNCTIONS, []);

        foreach ($exportedFunctions as $exportedName => $implementation) {
            $builder->addFunction($exportedName, $implementation);
        }

        return $builder->build();
    }

    /**
     * @throws InvalidConfigurationException
     */
    private function validatedPathTemplatesConfig(): array
    {
        $paths = $this->config->get(
            self::CONFIG_KEY_PATH_TEMPLATES,
            $this->config->get(TemplateEngineFactory::CONFIG_KEY_PATH_TEMPLATE)
        );

        if (! $paths) {
            throw new InvalidConfigurationException(self::CONFIG_KEY_PATH_TEMPLATES, 'Please configure the path to your templates. 
                E.g. [ \'path.template\' => projectPath(\'/templates\')] or 
                     [ \'path.templates\' => [ 
                        projectPath(\'/templates/A\'),
                        projectPath(\'/templates/B\'),  
                     ]');
        }

        if (is_array($paths)) {
            return $paths;
        }

        return [$paths];
    }

    /**
     * @throws InvalidConfigurationException if the config 'path.cache' is missing.
     */
    private function validatedPathCacheConfig(): string
    {
        $path = $this->config->get(TemplateEngineFactory::CONFIG_KEY_PATH_CACHE);
        if ($path) {
            return $path;
        }
        throw new InvalidConfigurationException(TemplateEngineFactory::CONFIG_KEY_PATH_CACHE, 'Please configure the path to your template cache. 
                      E.g. [\'path.cache\' => projectPath(\'/cache\')].');
    }
}
