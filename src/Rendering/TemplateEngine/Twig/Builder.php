<?php

namespace Macrominds\Rendering\TemplateEngine\Twig;

use Twig\Environment;
use Twig\Extra\Intl\IntlExtension;
use Twig\Loader\FilesystemLoader;
use Twig\TwigFunction;

class Builder
{
    /**
     * @var string
     */
    private $cachePath;
    /**
     * @var string[]
     */
    private $templatePaths = [];
    private $debugMode = false;
    private $functions = [];

    public function __construct(string $cachePath)
    {
        $this->cachePath = $cachePath;
    }

    /**
     * @param string[] $templatePaths
     *
     * @return Builder
     */
    public function setTemplatePaths(array $templatePaths): self
    {
        $this->templatePaths = $templatePaths;

        return $this;
    }

    public function getCachePath(): string
    {
        return $this->cachePath;
    }

    /**
     * @return string[]
     */
    public function getTemplatePaths(): array
    {
        return $this->templatePaths;
    }

    public function setDebugMode(bool $debugModeOn): self
    {
        $this->debugMode = $debugModeOn;

        return $this;
    }

    public function getDebugMode(): bool
    {
        return $this->debugMode;
    }

    public function addFunction(string $name, callable $function): self
    {
        $this->functions[$name] = $function;

        return $this;
    }

    /**
     * @return callable[]
     */
    public function getFunctions(): array
    {
        return $this->functions;
    }

    public function build(): Twig
    {
        $loader = new FilesystemLoader($this->templatePaths);
        $environment = new Environment($loader, ['cache' => $this->cachePath, 'debug' => $this->debugMode]);
        $this->addDefaultExtensions($environment);
        $this->addFunctions($environment);

        return new Twig($environment);
    }

    protected function addDefaultExtensions(Environment $environment): Environment
    {
        $environment->addExtension(new IntlExtension());

        return $environment;
    }

    protected function addFunctions(Environment $environment): Environment
    {
        foreach ($this->functions as $name => $function) {
            $environment->addFunction(new TwigFunction($name, $function));
        }

        return $environment;
    }
}
