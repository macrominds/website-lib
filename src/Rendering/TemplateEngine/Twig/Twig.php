<?php

namespace Macrominds\Rendering\TemplateEngine\Twig;

use Error;
use Macrominds\Rendering\TemplateEngine\Context;
use Macrominds\Rendering\TemplateEngine\TemplateEngine;
use Macrominds\Rendering\TemplateEngine\TemplateNotFoundException;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

class Twig implements TemplateEngine
{
    /**
     * @var Environment
     */
    protected $environment;

    public function __construct($environment)
    {
        $this->environment = $environment;
    }

    /**
     * @throws TemplateNotFoundException
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function render(Context $context, string $contentType): string
    {
        if ($context->getIsTemplate()) {
            $context->setContent($this->renderTemplateOrFail($context->getContent(), $context));
        }

        $templateFile = $this->locateTemplateFile(
            $context->getTemplate(),
            $contentType
        );

        return $this->renderFileOrFail(
            $templateFile,
            $context
        );
    }

    public function locateTemplateFile(string $name, string $contentType): string
    {
        return "{$name}.{$contentType}.twig";
    }

    /**
     * @throws RuntimeError
     * @throws SyntaxError
     * @throws TemplateNotFoundException
     */
    public function renderFileOrFail(string $templateFile, Context $context): string
    {
        try {
            return $this->renderFile($templateFile, $context);
        } catch (LoaderError $error) {
            throw new TemplateNotFoundException($templateFile, $error);
        }
    }

    /**
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function renderFile(string $templateFile, Context $context): string
    {
        return $this->environment->render($templateFile, $context->toArray());
    }

    /**
     * @throws SyntaxError
     * @throws TemplateNotFoundException
     * @throws RuntimeError
     */
    public function renderTemplateOrFail(string $template, Context $context): string
    {
        try {
            return $this->renderTemplate($template, $context);
        } catch (LoaderError $error) {
            // TODO research usecases for a LoaderError.
            // TODO Currently it looks as if Twig should not raise LoaderError for source templates.
            throw new TemplateNotFoundException('template from source', $error);
        }
    }

    /**
     * @throws SyntaxError
     * @throws LoaderError
     * @throws RuntimeError
     */
    public function renderTemplate(string $template, Context $context): string
    {
        try {
            return $this
                ->environment
                ->createTemplate($template)
                ->render($context->toArray());
        } catch (SyntaxError | LoaderError $e) {
            // These expected errors shall be passed through
            throw $e;
        } catch (Error $e) {
            $this->wrapInRuntimeError($e);
        }
    }

    /**
     * @throws RuntimeError
     */
    private function wrapInRuntimeError(Error $e): void
    {
        // php 7.4 throws an Error if we have an object to string conversion (echo $object in compiled template)
        // and \Twig\Template::displayWithErrorHandling with separation of Error and Exception.
        //
        // php 7.3 throws a twig RuntimeError (most likely, because it tries to convert the object to string
        // and fails afterwards. So this branch should only be active with php 7.4
        $e = new RuntimeError(
            sprintf('An exception has been thrown during the rendering of a template ("%s").',
                $e->getMessage()),
            -1,
            null,
            null /* <- we don't have an Exception, but an Error */
        );
        $e->guess();

        throw $e;
    }
}
