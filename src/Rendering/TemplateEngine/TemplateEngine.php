<?php

namespace Macrominds\Rendering\TemplateEngine;

interface TemplateEngine
{
    /**
     * @throws TemplateNotFoundException
     */
    public function render(Context $context, string $contentType): string;
}
