<?php

namespace Macrominds\Rendering\TemplateEngine;

use ArrayAccess;

class Context implements ArrayAccess
{
    private $data = [];

    /**
     * Context constructor.
     */
    public function __construct(array $data = [])
    {
        $this->data = $data;
    }

    public function setContent(string $content): Context
    {
        $this->data['content'] = $content;

        return $this;
    }

    public function getContent(): string
    {
        return $this->data['content'] ?? '';
    }

    public function setTemplate(string $template): Context
    {
        $this->data['template'] = $template;

        return $this;
    }

    public function getTemplate(): string
    {
        return $this->data['template'];
    }

    public function setIsTemplate(bool $flag): Context
    {
        $this->data['is_template'] = $flag;

        return $this;
    }

    public function getIsTemplate(): bool
    {
        return $this->data['is_template'] ?? false;
    }

    public function toArray(): array
    {
        return $this->data;
    }

    /**
     * Whether a offset exists.
     *
     * @see https://php.net/manual/en/arrayaccess.offsetexists.php
     *
     * @param mixed $offset <p>
     *                      An offset to check for.
     *                      </p>
     *
     * @return bool true on success or false on failure.
     *              </p>
     *              <p>
     *              The return value will be casted to boolean if non-boolean was returned.
     *
     * @since 5.0.0
     */
    public function offsetExists($offset): bool
    {
        return isset($this->data[$offset]);
    }

    /**
     * Offset to retrieve.
     *
     * @see https://php.net/manual/en/arrayaccess.offsetget.php
     *
     * @param mixed $offset <p>
     *                      The offset to retrieve.
     *                      </p>
     *
     * @return mixed can return all value types
     *
     * @since 5.0.0
     */
    public function offsetGet($offset): mixed
    {
        return $this->data[$offset];
    }

    /**
     * Offset to set.
     *
     * @see https://php.net/manual/en/arrayaccess.offsetset.php
     *
     * @param mixed $offset <p>
     *                      The offset to assign the value to.
     *                      </p>
     * @param mixed $value  <p>
     *                      The value to set.
     *                      </p>
     *
     * @return void
     *
     * @since 5.0.0
     */
    public function offsetSet($offset, $value): void
    {
        $this->data[$offset] = $value;
    }

    /**
     * Offset to unset.
     *
     * @see https://php.net/manual/en/arrayaccess.offsetunset.php
     *
     * @param mixed $offset <p>
     *                      The offset to unset.
     *                      </p>
     *
     * @return void
     *
     * @since 5.0.0
     */
    public function offsetUnset($offset): void
    {
        unset($this->data[$offset]);
    }
}
