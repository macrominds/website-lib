<?php

namespace Macrominds\Rendering;

interface ContentRenderer
{
    public function render(string $string): string;
}
