<?php

namespace Macrominds\Rendering;

use ParsedownExtra;

class MarkdownRenderer implements ContentRenderer
{
    public function render(string $string): string
    {
        return (new ParsedownExtra())->text($string);
    }
}
