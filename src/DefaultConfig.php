<?php

namespace Macrominds;

use Macrominds\App\HasContainer;
use Macrominds\App\ProjectPathProvider;
use Macrominds\Config\CommonConfig;
use Macrominds\Config\Env;
use Macrominds\ContentAccess\ContentPathProvider;
use Macrominds\Helpers\Arr;
use Macrominds\Logger\LoggerFactory;
use Macrominds\Rendering\TemplateEngine\TemplateEngineFactory;
use Macrominds\Services\Container;
use Macrominds\YamlParser\YamlParserFactory;

class DefaultConfig
{
    use HasContainer;

    /**
     * @var ProjectPathProvider
     */
    private $projectPathProvider;

    public function __construct(Container $container = null)
    {
        $this->container = $this->provideContainer($container);
        $this->projectPathProvider = $this->container->resolve(ProjectPathProvider::class);
    }

    public function merge(array $override = []): array
    {
        return Arr::replaceRecursive($this->get(), $override);
    }

    public function get(): array
    {
        return [
            CommonConfig::KEY_APP_ENV => $this->getEnv(CommonConfig::KEY_APP_ENV, 'production'),

            ContentPathProvider::CONFIG_KEY_PATH_CONTENT => $this->projectPath('/content'),
            TemplateEngineFactory::CONFIG_KEY_PATH_TEMPLATE => $this->projectPath('/templates'),
            TemplateEngineFactory::CONFIG_KEY_PATH_CACHE => $this->projectPath('/cache'),
            LoggerFactory::CONFIG_KEY_PATH_LOGGING          => $this->projectPath('/log/mwf.log'),

            CommonConfig::KEY_SUFFIX_CONTENT_FILE           => 'yml.md',
            CommonConfig::KEY_DEFAULT_RESPONSE_TYPE         => 'html',

            YamlParserFactory::CONFIG_KEY_VARIABLES => [
                'template' => 'standard',
            ],
        ];
    }

    private function getEnv(string $key, $default = null)
    {
        return $this->resolveEnv()->get($key, $default);
    }

    private function resolveEnv(): Env
    {
        return $this->container->resolve(Env::class);
    }

    private function projectPath(string $string): string
    {
        return $this->projectPathProvider->getProjectPath($string);
    }
}
