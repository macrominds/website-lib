<?php

namespace Macrominds;

use Exception;
use Throwable;

class MissingProjectPathProviderException extends Exception
{
    public function __construct($message = '', $code = 0, Throwable $previous = null)
    {
        $messageWithInstruction = $this->buildMessageWithInstruction($message);
        parent::__construct($messageWithInstruction, $code, $previous);
    }

    private function buildMessageWithInstruction(string $message)
    {
        $instruction = self::getInstruction();

        return "{$message} {$instruction}";
    }

    public static function getInstruction()
    {
        return <<<'PHP'
use Macrominds\App\DefaultProjectPathProvider;
use Macrominds\App\ProjectPathProvider;
use Macrominds\BasicServiceProvider;
use Macrominds\Services\Container;
use Pimple\Container as PimpleContainer;

$container = (new Container(new PimpleContainer()))->registerServiceProvider(new BasicServiceProvider([
//...
]));
$container->singleton(ProjectPathProvider::class, function () {
    return new DefaultProjectPathProvider(realpath(__DIR__.'..'));
});
PHP;
    }
}
