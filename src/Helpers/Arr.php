<?php

namespace Macrominds\Helpers;

class Arr
{
    /**
     * @param null $fallback
     *
     * @return mixed
     */
    public static function get(array $array, string $key, $fallback = null)
    {
        if (! isset($array[$key]) && false !== strpos($key, '.')) {
            $keys = explode('.', $key, 2);
            $r = $array[$keys[0]] ?? null;
            if (is_array($r)) {
                return self::get($r, $keys[1], $fallback);
            }
        }

        return $array[$key] ?? $fallback;
    }

    public static function replaceRecursive(array $original, array $new): array
    {
        return array_replace_recursive($original, $new);
    }
}
