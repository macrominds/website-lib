<?php

namespace Macrominds\Helpers\Str;

class Studly
{
    /**
     * @var string
     */
    private $stringToTransform;

    public function __construct(string $string)
    {
        $this->stringToTransform = $string;
    }

    public function transform(): string
    {
        return str_replace(
            ' ',
            '',
            ucwords(
                str_replace(
                    ['-', '_'],
                    ' ',
                    $this->stringToTransform
                )
            )
        );
    }
}
