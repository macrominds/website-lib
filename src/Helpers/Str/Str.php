<?php

namespace Macrominds\Helpers\Str;

class Str
{
    public static function studly(string $string): string
    {
        return (new Studly($string))->transform();
    }

    public static function startsWith(string $haystack, string $needle): bool
    {
        return (new StartsWith($haystack))->check($needle);
    }
}
