<?php

namespace Macrominds\Helpers\Str;

use InvalidArgumentException;

class StartsWith
{
    /**
     * @var string
     */
    private $stringToCheck;

    public function __construct(string $stringToCheck)
    {
        $this->stringToCheck = $stringToCheck;
    }

    public function check(string $candidate): bool
    {
        if (empty($candidate)) {
            throw new InvalidArgumentException('The needle must not be empty.');
        }

        return 0 === strpos($this->stringToCheck, $candidate);
    }
}
