<?php

namespace Macrominds\Helpers;

class PathInfo
{
    /**
     * @var array
     */
    private $path;
    /**
     * @var string
     */
    private $prefix;
    /**
     * @var string
     */
    private $suffix;

    public function __construct(string $path)
    {
        $this->path = $path;
        $this->initializePrefixAndSuffix();
    }

    protected function initializePrefixAndSuffix()
    {
        $parts = explode('.', $this->path, 2);
        $this->prefix = $parts[0];
        $this->suffix = $parts[1] ?? null;
    }

    public function getSuffix(): ?string
    {
        return $this->suffix;
    }

    public function getFilenameWithoutSuffix()
    {
        return $this->prefix;
    }
}
