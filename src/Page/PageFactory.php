<?php

namespace Macrominds\Page;

use Macrominds\ContentAccess\ContentPathDoesNotExistException;
use Macrominds\Rendering\TemplateEngine\TemplateNotFoundException;
use Macrominds\Resource\ContentResource;

interface PageFactory
{
    /**
     * @throws ContentPathDoesNotExistException
     * @throws TemplateNotFoundException
     */
    public function createFrom(ContentResource $resource): Page;
}
