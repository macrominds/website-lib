<?php

namespace Macrominds\Page;

use Macrominds\Helpers\Arr;

class Page
{
    /**
     * @var string
     */
    private $content;
    /**
     * @var array
     */
    private $data;

    public function __construct(string $content, array $data)
    {
        $this->content = $content;
        $this->data = $data;
    }

    public function getContent(): string
    {
        return $this->content;
    }

    public function getData(): array
    {
        return $this->data;
    }

    public function data(string $key, $fallback = null)
    {
        return Arr::get($this->data, $key, $fallback);
    }
}
