<?php

namespace Macrominds\YamlParser;

use Macrominds\Parser\DataParser;
use Symfony\Component\Yaml\Exception\ParseException;
use Symfony\Component\Yaml\Yaml;

class YamlParser implements DataParser
{
    private $variables = [];

    public function __construct(array $variables = [])
    {
        $this->variables = $variables;
    }

    /**
     * @throws ParseException
     */
    public function parse(string $raw): array
    {
        $raw = $this->stripTripleDashes($raw);

        return array_merge($this->variables, Yaml::parse($raw, Yaml::PARSE_CUSTOM_TAGS));
    }

    private function stripTripleDashes(string $block): string
    {
        // multiple documents are not supported by the parser. Once it detects triple dashes (---), it will throw
        // a ParseException. So we will strip the dashes:
        return preg_replace("/^---\s*$/ms", "\n", $block);
    }
}
