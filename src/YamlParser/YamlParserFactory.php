<?php

namespace Macrominds\YamlParser;

use Macrominds\Config\Config;
use Macrominds\Config\InvalidConfigurationException;
use Macrominds\Services\Container;

class YamlParserFactory
{
    const CONFIG_KEY_VARIABLES = 'variables';
    /**
     * @var Config
     */
    private $config;

    public function __construct(Container $container)
    {
        $this->config = $container->resolve(Config::class);
    }

    /**
     * @throws InvalidConfigurationException
     */
    public function createYamlParser(): YamlParser
    {
        return new YamlParser($this->validatedYamlVariablesConfig());
    }

    /**
     * @throws InvalidConfigurationException if the config 'variables' is missing
     */
    private function validatedYamlVariablesConfig(): array
    {
        $variables = $this->config->get(self::CONFIG_KEY_VARIABLES);
        if ($variables) {
            return $variables;
        }
        throw new InvalidConfigurationException(self::CONFIG_KEY_VARIABLES, 'Please configure some variables for the Yaml Parser. E.g. [\'template\' => \'standard\'].');
    }
}
