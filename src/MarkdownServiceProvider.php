<?php

namespace Macrominds;

use Macrominds\Rendering\ContentRenderer;
use Macrominds\Rendering\MarkdownRenderer;
use Macrominds\Services\AppServiceProvider;
use Macrominds\Services\Container;

class MarkdownServiceProvider extends AppServiceProvider
{
    public function getSingletonServices(Container $c): array
    {
        return [
            ContentRenderer::class => function () {
                return new MarkdownRenderer();
            },
        ];
    }

    public function getFactoryObjectServices(Container $c): array
    {
        return [];
    }
}
