# Change Log

All noteable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/).

## [1.0.0] - 2023-09-17 „Fountain of youth“

- build(php)!: drop php7 support and add php8.1 and php8.2 support

## [0.7.1] – 2021-02-09 „Modern Working“

- Prevent dotEnv override
- Fix php 8.0 compatibility

## [0.7.0] – 2020-05-23 „Independent Flyweight“

- Extract macrominds/app to be able to setup other applications easily

## [0.6.0] – 2020-04-26 „Good Nomens“

- Replaced names like Procesing, Processor, Process with less generic and more appropriate names that reveal the intention better
- Improved readability
- The link `phpunit` has been removed to prevent cluttering the source directory. Call `vendor/bin/phpunit` or use an alias instead.

## [0.5.0] – 2020-01-15 „Tidy dumb down“

Added phpmetrics report script to simplify maintaining quality code in the future.
Also simplified the code and improved package relations.

- Fix duplicate setup and teardown of tests

## [0.4.0] – 2020-01-14 „Continuously Inverted Shape Shifter”

Improved Bootstrapping and Inversion of Control. Made compatible to PHP 7.4.

This MR includes breaking changes.

- Removed requirement for a user defined `getProjectRoot` function
- Removed requirement to include a bootstrap file
- Removed app() to favor manual instantiation and simplicity over magic and complexity
- Added the ability to **specify and overwrite singleton and non singleton bindings**
- Added the ability to **manually register ServiceProviders**
- Updated documentation to reflect the changes
- Made **compatible to php7.4**
- Updated dependencies
- Respond with 404 for non existing template types (was 500 before, which might have confused people)
- **CI**: Added Continuous Integration Pipelines for php 7.3 and php 7.4

## [0.3.1] – 2019-11-30 „Lazy Instructor“

- Extracted macrominds/website from this lib and changed the instructions on how to setup a new website

## [0.3.0] – 2019-11-30 „Lazy Builder“

### Changes

- Provided default configuration, so that a new user is not as overwhelmed anymore

### Breaking change

**Notice:** As this change included a gitlab repo url modification and a composer project rename, 
you cannot install pre 0.3.0 versions anymore with composer.

- Renamed the project from `macrominds/website` to `macrominds/website-lib` to reflect the intent better
  - This allows for a new project `macrominds/website`, so that we can then use `composer create-project macrominds/website mywebsite`
    However, we're not there yet. The `macrominds/website` project doesn't exist yet. 
    There will be another release with an updated README once it's there. 

## [0.2.0] – 2019-11-27 „Environmental Stem“

### Changes:

- Now using .env file for environment specific settings (such as `APP_ENV` to decide whether to expose errors or not)
- Improved log output with multiple lines and readable stack traces
- Now using Inversion of Control and a little bit of Dependency Injection

## [0.1.0] – 2019-11-21 „Alpha Puppy“

- early initial version
