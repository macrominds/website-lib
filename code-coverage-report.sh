#!/usr/bin/env bash

# We're running phpdbg here, because if we used php, that would require the user to turn on XDEBUG.
# Using phpdbg is far more convenient for the user.

# Evaluation of script directory taken from
# https://stackoverflow.com/questions/59895/get-the-source-directory-of-a-bash-script-from-within-the-script-itself
#
# DIR is only needed for the output.
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

# Run phpdbg
# See https://www.php.net/manual/en/intro.phpdbg.php
phpdbg -rr vendor/bin/phpunit --coverage-html=report/coverage \
  || echo "Tests where not successful. Report might have been generated anyways. See: " \
  && echo "Report has been generated. See: "

echo ""
echo "file://${DIR}/report/coverage/index.html"
echo ""

